﻿using Blaze.DataBridge.Entities;

namespace Blaze.DataBridge
{
    public interface IDataBridge
    {
        Match SaveOrUpdateMatch(Match match);
        Game SaveOrUpdateServer(Game game);
        Game GetGameById(ulong id);
        User SaveOrUpdateUser(User match);
        User GetUserByEmail(string email);
        User GetUserByEmailAndPassword(string email, string passwordHash);
        User GetUserByPersonaId(ulong personaId);
        User GetUserById(ulong id);
        void DeleteGameById(ulong id);
    }
}