﻿using Blaze.Common;

namespace Blaze.DataBridge
{
    public class IdentityObject
    {
        public IdentityObject()
        {
            Id = RandomGenerator.Long();
        }

        public long Id { get; set; }
    }
}