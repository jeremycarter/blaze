﻿using Blaze.Networking;

namespace Blaze.DataBridge.Entities
{
    public class User
    {
        public ulong Id { get; set; }
        public ClientType ClientType { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
    }
}