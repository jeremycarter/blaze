﻿using System;
using System.Net;

namespace Blaze.DataBridge.Entities
{
    public class Game
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
        public IPEndPoint ExternalEndPoint { get; set; }
        public IPEndPoint InternalEndPoint { get; set; }
        public Guid Guid { get; set; }
    }
}