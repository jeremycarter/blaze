﻿using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Blaze.Common.Extensions;
using Nancy.Owin;
using Nowin;

namespace Blaze.Networking.Https
{
    public class HttpsServer
    {
        private readonly INowinServer _server;

        public HttpsServer(X509Certificate2 certificate)
        {
            var myNancyAppFunc = NancyMiddleware.UseNancy()(env => Task.FromResult(0));

            _server = ServerBuilder.New()
                .SetOwinApp(myNancyAppFunc)
                .SetExecutionContextFlow(ExecutionContextFlow.SuppressAlways)
                .SetEndPoint(new IPEndPoint(IPAddress.Any, 443))
                .SetCertificate(certificate)
                .Build();
        }

        public void Start()
        {
            Task.Run(() => _server.Start()).Continue();
        }
    }
}