﻿using System.IO;
using Blaze.Common;
using Nancy;

namespace Blaze.Networking.Https
{
    public class UserModule : NancyModule
    {
        public UserModule()
        {
            this.EnableCors();

            Get["/api/pc/token/{version}/identity"] = _ =>
            {
                Log.Info("Request was made for user identity from " + Request.UserHostAddress);

                var json = "{}";
                return Response.AsText(json, "application/json");
            };

            Get["/api/bf4/pc/persona/{version}/{userId}/ingame_metadata"] = x =>
            {
                Log.Info("Request was made for user metadata from " + Request.UserHostAddress);

                var userId = (int) x.userId;
                var json = "{\"clubRank\": \"\" ,\"personaId\": " + userId + ",\"emblemUrl\": \"\",\"clubName\": \"\",\"countryCode\": \"US\"}";
                return Response.AsText(json, "application/json");
            };

            Get["/api/bf4/pc/battledash/{id}/widgetdata/{userId}"] = _ =>
            {
                Log.Info("Request was made for user widget data from " + Request.UserHostAddress);

                var json = "{\"activeMissions\": 0,\"npsActive\": false,\"battledashActive\": true,\"friendsPlaying\": 0,\"friendListHash\": \"\"}";
                return Response.AsText(json, "application/json");
            };
        }
    }
}