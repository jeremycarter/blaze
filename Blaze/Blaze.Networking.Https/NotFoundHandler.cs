﻿using Nancy;
using Nancy.ErrorHandling;

namespace Blaze.Networking.Https
{
    public class PageNotFoundHandler : IStatusCodeHandler
    {
        public bool HandlesStatusCode(HttpStatusCode statusCode, NancyContext context)
        {
            return statusCode == HttpStatusCode.NotFound;
        }

        public void Handle(HttpStatusCode statusCode, NancyContext context)
        {
            context.Response.StatusCode = HttpStatusCode.NotFound;
        }
    }
}