﻿using System.IO;
using Blaze.Common;
using Nancy;

namespace Blaze.Networking.Https
{
    public class ServerModule : NancyModule
    {
        public ServerModule()
        {
            this.EnableCors();

            Get["/battlefield/4/config/pc/server.xml"] = _ =>
            {
                Log.Info("Request was made for server.xml from " + Request.UserHostAddress);

                var str = File.ReadAllText(@"Data\BF4\server.xml");

                return Response.AsText(str, "text/xml");
            };

            Get["/battlefield/4/config/pc/game.xml"] = _ =>
            {
                Log.Info("Request was made for game.xml from " + Request.UserHostAddress);

                var str = File.ReadAllText(@"Data\BF4\game.xml");

                return Response.AsText(str, "text/xml");
            };
        }
    }
}