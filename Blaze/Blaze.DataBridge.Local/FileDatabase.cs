﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LiteDB;

namespace Blaze.DataBridge.Local
{
    public class FileDatabase
    {
        public FileDatabase()
        {
            _fileName = Path.Combine(Environment.CurrentDirectory + "blaze.db");
            _db = new LiteDatabase(_fileName);
        }

        private string _fileName { get; }
        private LiteDatabase _db { get; set; }

        public Task<T> FirstOrDefaultAsync<T>(Expression<Func<T, bool>> predicate) where T : new()
        {
            return Task.FromResult(GetCollection<T>().FindOne(predicate));
        }

        public Task<List<T>> WhereAsync<T, TKey>(Expression<Func<T, bool>> predicate) where T : new()
        {
            return Task.FromResult(GetCollection<T>().Find(predicate).ToList());
        }

        public Task<List<T>> FindAllAsync<T>() where T : new()
        {
            return Task.FromResult(GetCollection<T>().FindAll().ToList());
        }

        public LiteCollection<T> GetCollection<T>() where T : new()
        {
            var type = typeof (T);
            return _db.GetCollection<T>(type.Name + "Collection");
        }

        public void Insert<T>(T item) where T : new()
        {
            var collection = GetCollection<T>();
            collection.Insert(item);
        }

        public void Update<T>(T item) where T : new()
        {
            var collection = GetCollection<T>();
            collection.Update(item);
        }

        public void Delete(IdentityObject item)
        {
            var collection = GetCollection<IdentityObject>();
            collection.Delete(i => i.Id == item.Id);
        }

        public void Dispose()
        {
            _db.Dispose();
            _db = null;
        }
    }
}