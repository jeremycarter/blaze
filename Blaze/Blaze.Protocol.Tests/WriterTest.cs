﻿using System.Collections.Generic;
using Blaze.Protocol.Types;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Blaze.Protocol.Tests
{
    [TestClass]
    public class WriterTest
    {
        [TestMethod]
        public void ShouldWriteGenericPacket1()
        {
            var temp = new BlazePacket();
            temp.Component = 4;
            temp.Command = 20;
            temp.Error = 0;
            temp.MessageType = 8192;
            temp.Id = 0;

            var data = new TdfContainer();

            var game = new TdfStruct("GAME", new TdfContainer
            {
                new TdfList<long>("ADMN", TdfType.Integer, new List<long>
                {
                    714622916
                }),
                new TdfMap("ATTR", TdfType.String, TdfType.String, new SortedDictionary<object, object>
                {
                    {"bannerurl", ""},
                    {"changelist", "177035"},
                    {"country", "AQ"},
                    {"description1", ""},
                    {"description2", ""},
                    {"experience", ""},
                    {"fairfight", "NO"},
                    {"hash", "9B753AC1-E98C-CC3E-B3E4-777C38FC84C7"},
                    {"level", "MP_Siege"},
                    {"levellocation", "ConquestSmall0"},
                    {
                        "maps1",
                        "MP_Siege,CQS0;XP2_001,CQS0;MP_Resort,CQS0;XP3_UrbanGdn,CQS0;MP_Damage,CQS0;XP7_Valley,CQS0;XP1_001,CQS0;XP3_Prpganda,CQS0;XP0_Oman,CQS0;XP5_Night_01,CQS0;XP6_CMP,CQS0;MP_Journey,CQS0;XP0_Firestorm,CQS0;XP3_WtrFront,CQS0;XP1_004,CQS0;XP2_004,CQS0;XP0_Metro"
                    },
                    {
                        "maps2",
                        ",CQS0;XP0_Caspian,CQS0;XP4_Titan,CQS0;XP2_003,CQS0;MP_Prison,CQS0;XP3_MarketPl,CQS0;XP4_SubBase,CQS0;XP4_WlkrFtry,CQS0;MP_Tremors,CQS0;XP2_002,CQS0;XP1_002,CQS0;XP1_003,CQS0;MP_Flooded,CQS0;MP_Naval,CQS0;MP_TheDish,CQS0;MP_Abandoned,CQS0;XP4_Arctic,CQS0;"
                    },
                    {"mapsinfo", "0,1;0,1"},
                    {"message", ""},
                    {"mode", "ConquestSmall"},
                    {"preset", "NORMAL"},
                    {"providerid", "5230fd26a887040767289e312dc15e2106a42c0ffdc33e29"},
                    {"punkbuster", "NO"},
                    {"punkbusterversion", ""},
                    {"region", "EU"},
                    {"servertype", "OFFICIAL"},
                    {
                        "settings1",
                        "gmwp=0;vbdm=100;vaba=true;vrhe=true;vnta=true;vicc=false;vfrm=false;vtkk=3;vprp=33;osls=false;aaro=false;vprt=100;vhit=true;vtkc=5;vrtl=100;vtbr=100;vinb=false;vvsd=100;vcmd=true;vrsp=4;vmpl=0;vvsa=true;vkca=true;aasl=false;vprc=1;vgmc=100;vprb=180;vnit=3"
                    },
                    {
                        "settings2",
                        "00;v3sp=true;vsbb=true;vmst=4;vffi=false;vrlc=15;vhud=true;vnip=22;v3ca=true;vshe=100;vmsp=true;vmin=true;"
                    },
                    {"tickRate", "120"},
                    {"tickRateMax", "120"},
                    {"type", "mp"}
                }),
                new TdfList<long>("CAP", TdfType.Integer, new List<long>
                {
                    34,
                    0,
                    4,
                    0
                }),
                new TdfInteger("GID", 1),
                new TdfInteger("GMRG", 0),
                new TdfString("GNAM", "Test server"),
                new TdfInteger("GPVH", 6667),
                new TdfInteger("GSET", 1327),
                new TdfInteger("GSID", 1337),
                new TdfInteger("GSTA", 1),
                new TdfString("GTYPE", "frostbite_multiplayer"),
                new TdfString("GURL", "")
            });

            game.Values.Add(new TdfList<TdfStruct>("HNET", TdfType.Struct,
                new List<TdfStruct>
                {
                    new TdfStruct("VALU", new List<ITdf>
                    {
                        new TdfStruct("EXIP", new List<ITdf>
                        {
                            new TdfInteger("IP", 3232235883),
                            new TdfInteger("PORT", 10071)
                        }),
                        new TdfStruct("INIP", new List<ITdf>
                        {
                            new TdfInteger("IP", 3232235883),
                            new TdfInteger("PORT", 10071)
                        })
                    }, true)
                }));


            game.Values.AddRange(new List<ITdf>
            {
                new TdfInteger("HSES", 13666),
                new TdfInteger("IGNO", 0),
                new TdfInteger("MCAP", 34),
                new TdfInteger("NRES", 1),
                new TdfInteger("NTOP", 1),
                new TdfString("PGID", "b6852db1-ba37-4b40-aea3-0bd16efba4f9"),
                new TdfBinary("PGSR",
                    new byte[]
                    {
                        116, 26, 165, 219, 0, 218, 115, 13, 171, 131, 122, 136, 242, 8, 143, 174, 248, 102, 47, 139, 67,
                        224, 154, 76, 52, 215, 66, 30, 77, 151, 126, 25, 149, 141, 85, 185, 208, 37, 64, 143, 64, 130,
                        172, 214, 247, 13, 102, 144, 255, 235, 127, 228, 55, 199, 249, 5
                    })
            }
                );

            game.Values.AddRange(new List<ITdf>
            {
                new TdfStruct("PHST", new List<ITdf>
                {
                    new TdfInteger("HPID", 714622916),
                    new TdfInteger("HSLT", 1)
                }),
                new TdfInteger("PRES", 1),
                new TdfString("PSAS", "ams"),
                new TdfInteger("QCAP", 5)
            });

            game.Values.Add(new TdfUnion("REAS", 0,
                new TdfStruct("VALU", new List<ITdf>
                {
                    new TdfInteger("DCTX", 0)
                })));

            game.Values.AddRange(new List<ITdf>
            {
                new TdfInteger("SEED", 11181),
                new TdfInteger("TCAP", 0)
            });

            game.Values.AddRange(new List<ITdf>
            {
                new TdfStruct("THST", new List<ITdf>
                {
                    new TdfInteger("HPID", 1),
                    new TdfInteger("HSLT", 0)
                })
            });

            game.Values.AddRange(new List<ITdf>
            {
                new TdfString("UUID", "7832b291-41a5-4e6b-b431-118594721c64"),
                new TdfInteger("VOIP", 1),
                new TdfString("VSTR", "4900")
            });


            data.Add(game);
            temp.Content = data;

            var writer = new BlazePacketWriter();
            var bytes = writer.ToBytes(temp);

            Assert.AreNotEqual(0, bytes.Length);

            var reader = new BlazeStreamParser();
            var packet = reader.Parse(bytes);

            Assert.IsNotNull(packet);
            Assert.IsNotNull(packet.Content);
            Assert.AreNotEqual(0, packet.Content.Count);
            Assert.IsTrue(bytes.Length > 100);
        }

        [TestMethod]
        public void ShouldWriteGenericPacket2()
        {
            var temp = new BlazePacket();
            temp.Component = 9;
            temp.Command = 7;
            temp.Error = 0;
            temp.MessageType = 4096;
            temp.Id = 0;

            var data = new TdfContainer();
            data.Add(new TdfInteger("ANON", 0));
            data.Add(new TdfString("ASRC", "300294"));
            data.Add(new TdfList<long>("CIDS", TdfType.Integer,
                new long[] {1, 25, 4, 27, 28, 6, 7, 9, 10, 11, 30720, 30721, 30722, 30723, 20, 30725, 30726, 2000}));
            data.Add(new TdfString("CNGN", ""));
            data.Add(new TdfStruct("CONF", new List<ITdf>
            {
                new TdfMap("CONF", TdfType.String, TdfType.String,
                    new SortedDictionary<object, object>
                    {
                        {"associationListSkipInitialSet", "1"},
                        {"blazeServerClientId", "GOS-BlazeServer-BF4-PC"},
                        {"bytevaultHostname", "bytevault.gameservices.ea.com"},
                        {"bytevaultPort", "42210"},
                        {"bytevaultSecure", "false"},
                        {"capsStringValidationUri", "client-strings.xboxlive.com"},
                        {"connIdleTimeout", "90s"},
                        {"defaultRequestTimeout", "60s"},
                        {"identityDisplayUri", "console2/welcome"},
                        {"identityRedirectUri", "http://127.0.0.1/success"},
                        {"nucleusConnect", "http://127.0.0.1"},
                        {"nucleusProxy", "http://127.0.0.1/"},
                        {"pingPeriod", "20s"},
                        {"userManagerMaxCachedUsers", "0"},
                        {"voipHeadsetUpdateRate", "1000"},
                        {"xblTokenUrn", "http://127.0.0.1"},
                        {"xlspConnectionIdleTimeout", "300"}
                    })
            }));

            data.Add(new TdfString("INST", "battlefield-4-pc"));
            data.Add(new TdfInteger("MINR", 0));
            data.Add(new TdfString("NASP", "cem_ea_id"));
            data.Add(new TdfString("PLAT", "pc"));

            data.Add(new TdfStruct("QOSS", new List<ITdf>
            {
                new TdfStruct("BWPS", new List<ITdf>
                {
                    new TdfString("PSA", "192.168.1.107"),
                    new TdfInteger("PSP", 17502),
                    new TdfString("SNA", "rs-prod-lhr-bf4")
                }),
                new TdfInteger("LNP", 10),
                new TdfMap("LTPS", TdfType.String, TdfType.Struct, new SortedDictionary<object, object>
                {
                    {
                        "ams", new TdfStruct("VALU", new List<ITdf>
                        {
                            new TdfString("PSA", "192.168.1.107"),
                            new TdfInteger("PSP", 17502),
                            new TdfString("SNA", "rs-prod-lhr-bf4")
                        })
                    }
                }),
                new TdfInteger("SVID", 1337)
            }));

            data.Add(new TdfString("RSRC", "302123"));
            data.Add(new TdfString("SVER", "Blaze 13.15.08.0 (CL# 9442625)"));

            temp.Content = data;

            var writer = new BlazePacketWriter();
            var bytes = writer.ToBytes(temp);

            Assert.AreEqual(880, bytes.Length);

            var reader = new BlazeStreamParser();
            var packet = reader.Parse(bytes);

            Assert.IsNotNull(packet);
            Assert.IsNotNull(packet.Content);
            Assert.AreNotEqual(0, packet.Content.Count);
        }
    }
}