﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Blaze.Protocol.Tests
{
    [TestClass]
    public class MapNotificationPacketTest
    {
        private int _length;
        private string _path;
        private BlazePacket _readPacket;

        [TestInitialize]
        public void Initialise()
        {
            _path = Path.Combine(Environment.CurrentDirectory, "Data", "Notification_GameManager_4.bin");
            Assert.AreEqual(true, File.Exists(_path));
        }

        [TestMethod]
        public void ShouldReadPacket()
        {
            var bytes = File.ReadAllBytes(_path);
            Assert.AreNotEqual(0, bytes.Length);
            _length = bytes.Length;

            var hex = bytes.ToHex();

            var parser = new BlazeStreamParser();
            _readPacket = parser.Parse(bytes);

            Assert.IsNotNull(_readPacket);
            Assert.AreEqual(1, _readPacket.Content.Count);
        }
    }
}