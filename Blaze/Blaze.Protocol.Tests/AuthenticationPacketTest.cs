﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Blaze.Protocol.Tests
{
    [TestClass]
    public class AuthenticationPacketTest
    {
        public int Length;
        public string Path;
        public BlazePacket ReadPacket;

        [TestInitialize]
        public void Initialise()
        {
            Path = System.IO.Path.Combine(Environment.CurrentDirectory, "Data", "Request_Authentication_60.bin");
            Assert.AreEqual(true, File.Exists(Path));
        }

        [TestMethod]
        public void ShouldReadPacket()
        {
            var bytes = File.ReadAllBytes(Path);
            Assert.AreNotEqual(0, bytes.Length);
            Length = bytes.Length;

            var parser = new BlazeStreamParser();
            ReadPacket = parser.Parse(bytes);

            Assert.IsNotNull(ReadPacket);
            Assert.AreEqual(3, ReadPacket.Content.Count);
        }

        [TestMethod]
        public void ShouldWritePacket()
        {
            ShouldReadPacket();

            var writer = new BlazePacketWriter();
            var bytes = writer.ToBytes(ReadPacket);

            Assert.AreNotEqual(0, bytes.Length);
            Assert.AreEqual(Length, bytes.Length);
        }
    }
}