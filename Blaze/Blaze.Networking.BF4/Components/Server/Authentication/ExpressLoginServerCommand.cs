﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Blaze.Common;
using Blaze.DataBridge;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.BF4.Notifications.UserSession;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Server.Authentication
{
    [Component((int) Component.Authentication)]
    [Command(60)]
    public class ExpressLoginServerCommand : ICommandHandler
    {
        private readonly IDataBridge _dataBridge;
        private readonly ClientSessionManager _sessionManager;

        public ExpressLoginServerCommand(ClientSessionManager clientManager, IDataBridge dataBridge)
        {
            _sessionManager = clientManager;
            _dataBridge = dataBridge;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            // Lookup the session that was created during the pre-auth

            var session = _sessionManager.Lookup(request);

            var email = packet.Content.GetString("MAIL");
            var password = packet.Content.GetString("PASS");

            Log.Request($"Server {request.Client.EndPoint} logging in with email {email}");

            Log.Warn("Server user not found - creating user");

            session.UserId = 100000;
            session.PersonaId = session.UserId;
            session.Email = email;
            session.Name = "Server" + session.Id;
            session.PasswordHash = Hasher.SHA1(password);

            // Update the player ticket to represent their userid
            session.Ticket = $"PlayerTicket_{session.UserId}";

            var data = new TdfContainer
            {
                new TdfInteger("AGUP", false),
                new TdfInteger("ANON", false),
                new TdfInteger("NTOS", false),
                new TdfString("PCTK", session.Ticket),
                new TdfStruct("SESS", new List<ITdf>
                {
                    new TdfInteger("BUID", session.UserId),
                    new TdfInteger("FRST", false),
                    new TdfString("KEY", session.Key),
                    new TdfInteger("LLOG", 1403663841),
                    new TdfString("MAIL", session.Email),
                    new TdfStruct("PDTL", new List<ITdf>
                    {
                        new TdfString("DSNM", session.Name),
                        new TdfInteger("LAST", 1403663841),
                        new TdfInteger("PID", session.UserId),
                        new TdfInteger("PLAT", (ulong) Platform.PC),
                        new TdfInteger("STAS", (ulong) Status.Active),
                        new TdfInteger("XREF", 0)
                    }),
                    new TdfInteger("UID", session.UserId)
                }),
                new TdfInteger("SPAM", false),
                new TdfInteger("UNDR", false)
            };

            request.Reply(data);

            request.Notify(new SessionCreatedNotification(session));
            request.Notify(new UserAddedNotification(session, 0, session.PersonaId));
            request.Notify(new UserUpdatedNotification(session));



        }
    }
}