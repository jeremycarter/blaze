﻿using System.Threading.Tasks;
using Blaze.DataBridge;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Server.Authentication
{
    [Component((int) Component.Authentication)]
    [Command(29)]
    public class ListUserEntitlementsServerCommand : ICommandHandler
    {
        private readonly IDataBridge _dataBridge;
        private readonly ClientSessionManager _sessionManager;

        public ListUserEntitlementsServerCommand(ClientSessionManager clientManager, IDataBridge dataBridge)
        {
            _sessionManager = clientManager;
            _dataBridge = dataBridge;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            var userId = packet.Content.GetInteger("BUID");
            var state = packet.Content.GetInteger("STAT");

            // Allow all BF4 premium content and expansion packs
            var data = new TdfContainer
            {
                new TdfHex(
                    "baccf404030a925da90101009e48790101009ee86d0106424634504300a640000001a738ef0000c299000000c2aa64010100c328e10002c32a64010d44523a32333536363334303000cf48740001cf4ca30000d219c0010e4f4e4c494e455f41434345535300d24879010100d39c250001d63bb40000da5c80000000925da90101009e48790101009ee86d0106424634504300a640000001a738ef0000c299000000c2aa64010100c328e10002c32a64010100cf48740001cf4ca30000d219c0010f5052454d49554d5f41434345535300d24879010100d39c250005d63bb40000da5c80000000925da90101009e48790101009ee86d0106424634504300a640000001a738ef0000c299000000c2aa64010100c328e10002c32a64010100cf48740001cf4ca30000d219c0010e585041434b305f41434345535300d24879010100d39c250005d63bb40000da5c80000000925da90101009e48790101009ee86d0106424634504300a640000001a738ef0000c299000000c2aa64010100c328e10002c32a64010100cf48740001cf4ca30000d219c0010e585041434b315f41434345535300d24879010100d39c250005d63bb40000da5c80000000925da90101009e48790101009ee86d0106424634504300a640000001a738ef0000c299000000c2aa64010100c328e10002c32a64010100cf48740001cf4ca30000d219c0010e585041434b325f41434345535300d24879010100d39c250005d63bb40000da5c80000000925da90101009e48790101009ee86d0106424634504300a640000001a738ef0000c299000000c2aa64010100c328e10002c32a64010100cf48740001cf4ca30000d219c0010e585041434b335f41434345535300d24879010100d39c250005d63bb40000da5c80000000925da90101009e48790101009ee86d0106424634504300a640000001a738ef0000c299000000c2aa64010100c328e10002c32a64010100cf48740001cf4ca30000d219c0010e585041434b345f41434345535300d24879010100d39c250005d63bb40000da5c80000000925da90101009e48790101009ee86d0106424634504300a640000001a738ef0000c299000000c2aa64010100c328e10002c32a64010100cf48740001cf4ca30000d219c0010e585041434b355f41434345535300d24879010100d39c250005d63bb40000da5c80000000925da90101009e48790101009ee86d0106424634504300a640000001a738ef0000c299000000c2aa64010100c328e10002c32a64010100cf48740001cf4ca30000d219c0010e585041434b365f41434345535300d24879010100d39c250005d63bb40000da5c80000000925da90101009e48790101009ee86d0106424634504300a640000001a738ef0000c299000000c2aa64010100c328e10002c32a64010100cf48740001cf4ca30000d219c0010e585041434b375f41434345535300d24879010100d39c250005d63bb40000da5c80000000")
            };

            request.Reply(data);
        }
    }
}