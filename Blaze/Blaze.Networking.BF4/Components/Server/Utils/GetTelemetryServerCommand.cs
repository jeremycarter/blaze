﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Blaze.Common;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Server.Utils
{
    [Component(Component.Util)]
    [Command(5)]
    [ClientType(ClientType.DedicatedServer)]
    public class GetTelemetryServerCommand : ICommandHandler
    {
        private readonly ClientSessionManager _sessionManager;

        public GetTelemetryServerCommand(ClientSessionManager sessionManager)
        {
            _sessionManager = sessionManager;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            Log.Request($"Server {request.Client.EndPoint} getting telemetry data");

            var session = _sessionManager.Lookup(request);
            if (session != null)
            {
                var data = new TdfContainer
                {
                    new TdfStruct("TELE", new List<ITdf>
                    {
                        new TdfString("ADRS", "gostelemetry.blaze3.ea.com"),
                        new TdfInteger("ANON", 0),
                        new TdfString("DISA", ""),
                        new TdfInteger("EDCT", 0),
                        new TdfString("FILT", "-GAME/COMM/EXPD"),
                        new TdfInteger("LOC", session.Localization),
                        new TdfInteger("MINR", 0),
                        new TdfString("NOOK", ""),
                        new TdfInteger("PORT", 9988),
                        new TdfInteger("SDLY", 15000),
                        new TdfString("SESS", "session_" + session.Id),
                        new TdfString("SKEY", "key_" + session.Id),
                        new TdfInteger("SPCT", 75),
                        new TdfString("STIM", "Default"),
                        new TdfString("SVNM", "telemetry-3-common")
                    }, false)
                };

                request.Reply(data);
            }
        }
    }
}