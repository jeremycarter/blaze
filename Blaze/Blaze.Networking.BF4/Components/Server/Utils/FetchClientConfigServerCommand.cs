﻿using System.Threading.Tasks;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking.BF4.Components.Server.Utils
{
    [Component(Component.Util)]
    [Command(1)]
    [ClientType(ClientType.DedicatedServer)]
    public class FetchClientConfigServerCommand : ICommandHandler
    {
        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            request.ReplyEmpty();
        }
    }
}