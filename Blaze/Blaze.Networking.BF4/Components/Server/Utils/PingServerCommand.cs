﻿using System.Threading.Tasks;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Server.Utils
{
    [Component(Component.Util)]
    [Command(2)]
    [ClientType(ClientType.DedicatedServer)]
    public class PingServerCommand : ICommandHandler
    {
        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            var data = new TdfContainer
            {
                new TdfInteger("STIM", Utilities.GetUnixTime())
            };

            request.Reply(data);
        }
    }
}