﻿using System.Threading.Tasks;
using Blaze.Common;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.BF4.Notifications.GameManager;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking.BF4.Components.Server.GameManager
{
    [Component(Component.GameManager)]
    [Command(3)]
    [ClientType(ClientType.DedicatedServer)]
    public class AdvanceGameStateServerCommand : ICommandHandler
    {
        private readonly GameSessionManager _gameManager;

        public AdvanceGameStateServerCommand(GameSessionManager gameManager)
        {
            _gameManager = gameManager;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            Log.Request($"Server {request.Client.EndPoint} advancing game state");

            var id = packet.Content.GetInteger("GID");
            var state = packet.Content.GetInteger("GSTA");

            var game = _gameManager.Lookup(id);

            game.State = (GameState) state;

            request.Reply();

            request.Notify(new GameStateChangeNotification(game));
        }
    }
}