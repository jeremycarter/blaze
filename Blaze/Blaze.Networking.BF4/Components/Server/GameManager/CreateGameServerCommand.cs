﻿using System;
using System.Net;
using System.Threading.Tasks;
using Blaze.Common;
using Blaze.Common.Extensions;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.BF4.Notifications.GameManager;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Server.GameManager
{
    [Component((ushort) Component.GameManager)]
    [Command(1)]
    public class CreateGameServerCommand : ICommandHandler
    {
        private readonly GameSessionManager _gameManager;
        private readonly ClientSessionManager _sessionManager;

        public CreateGameServerCommand(ClientSessionManager sessionManager, GameSessionManager gameManager)
        {
            _sessionManager = sessionManager;
            _gameManager = gameManager;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            Log.Request($"Server {request.Client.EndPoint} creating a game");

            var session = _sessionManager.Lookup(request);

            // Cleanup any existing game sessions
            _gameManager.Remove(session.InternalEndpoint);
            _gameManager.Remove(session.ExternalEndpoint);

            // Initially use the values from the client session
            var game = new GameSession
            {
                ClientSessionId = session.Id,
                InternalEndpoint = session.InternalEndpoint,
                ExternalEndpoint = session.ExternalEndpoint
            };

            // Get the values supplied from the request
            var attr = packet.Content.GetItem<TdfMap>("ATTR");
            var playerCapacity = packet.Content.GetItem<TdfList<long>>("PCAP");

            game.NotResettable = packet.Content.GetBoolean("NRES");

            game.Name = packet.Content.GetString("GNAM");
            game.Attributes = attr.Map;
            game.Capacity = playerCapacity.Values;

            game.Level = attr.GetValue<string>("level");
            game.Type = attr.GetValue<string>("levellocation");

            game.MaxPlayers = Convert.ToUInt16(playerCapacity.Values[0]);
            game.QueueCapacity = 5;
            game.PresenceMode = (PresenceMode) packet.Content.GetInteger("PRES");

            game.State = GameState.Initializing;

            game.NetworkTopology = GameNetworkTopology.ClientServerDedicated;
            game.VoipTopology = VoipTopology.DedicatedServer;

            game.Settings = packet.Content.GetInteger("GSET");

            // Attempt to see if the HNET data is usable
            try
            {
                var hnetItem = (TdfList<TdfStruct>) packet.Content.GetItem("HNET");
                var hnet = hnetItem.FirstOrDefault();

                var exip = hnet.Find<TdfStruct>(tdf => tdf.Label == "EXIP");
                var inip = hnet.Find<TdfStruct>(tdf => tdf.Label == "INIP");

                var externalIp = exip.Find<TdfInteger>(tdf => tdf.Label == "IP");
                var externalPort = exip.Find<TdfInteger>(tdf => tdf.Label == "PORT");

                var internalIp = inip.Find<TdfInteger>(tdf => tdf.Label == "IP");
                var internalPort = inip.Find<TdfInteger>(tdf => tdf.Label == "PORT");

                if (externalIp.Value != 0)
                {
                    var ip = externalIp.Value.ToIPAddress();
                    var port = Convert.ToInt32(externalPort.Value);
                    session.ExternalEndpoint = new IPEndPoint(ip, port);
                }

                if (internalIp.Value != 0)
                {
                    var ip = internalIp.Value.ToIPAddress();
                    var port = Convert.ToInt32(internalPort.Value);
                    session.InternalEndpoint = new IPEndPoint(ip, port);
                }
            }
            catch
            {
            }

            // Save the game session and get an id 
            _gameManager.StartTracking(game);

            var data = new TdfContainer
            {
                new TdfInteger("GID", game.Id)
            };

            request.Reply(data);

            request.Notify(new GameSetupNotification(session, game));
            request.Notify(new GameStateChangeNotification(game));
        }
    }
}