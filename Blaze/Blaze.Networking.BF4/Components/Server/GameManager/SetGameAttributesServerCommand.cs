﻿using System.Threading.Tasks;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking.BF4.Components.Server.GameManager
{
    [Component(Component.GameManager)]
    [Command(7)]
    [ClientType(ClientType.DedicatedServer)]
    public class SetGameAttributesServerCommand : ICommandHandler
    {
        private readonly GameSessionManager _gameManager;

        public SetGameAttributesServerCommand(GameSessionManager gameManager)
        {
            _gameManager = gameManager;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            request.Reply();
        }
    }
}