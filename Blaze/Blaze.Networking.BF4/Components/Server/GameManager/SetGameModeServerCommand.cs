﻿using System.Threading.Tasks;
using Blaze.Common;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.BF4.Notifications.GameManager;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking.BF4.Components.Server.GameManager
{
    [Component(Component.GameManager)]
    [Command(41)]
    [ClientType(ClientType.DedicatedServer)]
    public class SetGameModServerCommand : ICommandHandler
    {
        private readonly GameSessionManager _gameManager;

        public SetGameModServerCommand(GameSessionManager gameManager)
        {
            _gameManager = gameManager;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            Log.Request($"Server {request.Client.EndPoint} setting game mode");

            var id = packet.Content.GetInteger("GMID");
            var manager = packet.Content.GetInteger("GMRG");

            var game = _gameManager.Lookup(id);

            game.ManagerId = manager;

            request.Reply();

            request.Notify(new GameModChangedNotification(id, manager));
        }
    }
}