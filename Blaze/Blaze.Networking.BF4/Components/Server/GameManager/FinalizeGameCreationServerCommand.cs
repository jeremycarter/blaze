﻿using System.Threading.Tasks;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.BF4.Notifications.GameManager;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking.BF4.Components.Server.GameManager
{
    [Component((ushort) Component.GameManager)]
    [Command(15)]
    public class FinalizeGameCreationServerCommand : ICommandHandler
    {
        private readonly GameSessionManager _gameManager;
        private readonly ClientSessionManager _sessionManager;

        public FinalizeGameCreationServerCommand(GameSessionManager gameManager, ClientSessionManager sessionManager)
        {
            _gameManager = gameManager;
            _sessionManager = sessionManager;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            var id = packet.Content.GetInteger("GID");

            var game = _gameManager.Lookup(id);
            var session = _sessionManager.Lookup(request);

            request.Reply();

            request.Notify(new GameFinalizedNotification(game, session));
        }
    }
}