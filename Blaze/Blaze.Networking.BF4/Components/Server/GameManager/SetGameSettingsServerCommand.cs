﻿using System.Threading.Tasks;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.BF4.Notifications.GameManager;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking.BF4.Components.Server.GameManager
{
    [Component(Component.GameManager)]
    [Command(4)]
    [ClientType(ClientType.DedicatedServer)]
    public class SetGameSettingsServerCommand : ICommandHandler
    {
        private readonly GameSessionManager _gameManager;

        public SetGameSettingsServerCommand(GameSessionManager gameManager)
        {
            _gameManager = gameManager;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            var id = packet.Content.GetInteger("GID");
            var settings = packet.Content.GetInteger("GSET");

            var game = _gameManager.Lookup(id);

            game.Settings = settings;

            request.Reply();

            request.Notify(new GameSettingsChangeNotification(game));
        }
    }
}