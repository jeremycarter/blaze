﻿using System.Threading.Tasks;
using Blaze.DataBridge;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking.BF4.Components.Server.Inventory
{
    [Component(Component.Inventory)]
    [Command(3)]
    [ClientType(ClientType.DedicatedServer)]
    public class UseConsumableServerCommand : ICommandHandler
    {
        private readonly IDataBridge _dataBridge;
        private readonly ClientSessionManager _sessionManager;

        public UseConsumableServerCommand(ClientSessionManager clientManager, IDataBridge dataBridge)
        {
            _sessionManager = clientManager;
            _dataBridge = dataBridge;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            // Lookup the session that was created during the pre-auth

            var session = _sessionManager.Lookup(request);

            request.Reply();
        }
    }
}