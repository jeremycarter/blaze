﻿using Blaze.Common;
using Blaze.DataBridge;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking.BF4.Components.Server.UserSession
{
    [Component(Component.UserSessions)]
    [Command(35)]
    [ClientType(ClientType.DedicatedServer)]
    public class ResumeSessionServerCommand : ICommandHandler
    {
        private readonly IDataBridge _dataBridge;
        private readonly ClientSessionManager _sessionManager;

        public ResumeSessionServerCommand(ClientSessionManager clientManager, IDataBridge dataBridge)
        {
            _sessionManager = clientManager;
            _dataBridge = dataBridge;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            Log.Info($"Server {request.Client.EndPoint} resuming session");

            request.ReplyEmpty();
        }
    }
}