﻿using Blaze.DataBridge;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.BF4.Notifications.GameManager;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking.BF4.Components.Server.Statistics
{
    [Component(Component.Stats)]
    [Command(16)]
    [ClientType(ClientType.DedicatedServer)]
    public class GetStatsGroupAsyncServerCommand : ICommandHandler
    {
        private readonly IDataBridge _dataBridge;
        private readonly ClientSessionManager _sessionManager;

        public GetStatsGroupAsyncServerCommand(ClientSessionManager clientManager, IDataBridge dataBridge)
        {
            _sessionManager = clientManager;
            _dataBridge = dataBridge;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            var vId = packet.Content.GetInteger("VID");
            request.ReplyEmpty();
            request.Notify(new GetStatsGroupAsyncNotification(vId));
        }
    }
}