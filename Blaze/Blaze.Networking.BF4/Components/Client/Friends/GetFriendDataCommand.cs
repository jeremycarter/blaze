﻿using System.Threading.Tasks;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking.BF4.Components.Client.Friends
{
    [Component((ushort) Component.Friends)]
    [Command(6)]
    [ClientType(ClientType.GameplayUser)]
    public class GetFriendDataCommand : ICommandHandler
    {
        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            request.ReplyEmpty();
        }
    }
}