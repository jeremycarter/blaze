﻿using System.Threading.Tasks;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking.BF4.Components.Client.Response
{
    [Component(Component.Rsp)]
    [Command(50)]
    [ClientType(ClientType.GameplayUser)]
    public class ResponseCommand : ICommandHandler
    {
        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            request.ReplyEmpty();
        }
    }
}