﻿using System.IO;
using System.Threading.Tasks;
using Blaze.DataBridge;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Client.Inventory
{
    [Component(Component.Inventory)]
    [Command(6)]
    [ClientType(ClientType.GameplayUser)]
    public class GetTemplateCommand : ICommandHandler
    {
        private readonly IDataBridge _dataBridge;
        private readonly ClientSessionManager _sessionManager;

        public GetTemplateCommand(ClientSessionManager clientManager, IDataBridge dataBridge)
        {
            _sessionManager = clientManager;
            _dataBridge = dataBridge;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            // Lookup the session that was created during the pre-auth

            var inventory = new TdfList<string>("ILST", TdfType.String);
            var items = File.ReadAllLines(@"Data\BF4\items.txt");
            foreach (var testItem in items)
            {
                inventory.Values.Add(testItem);
            }

            var data = new TdfContainer
            {
                inventory
            };

            var session = _sessionManager.Lookup(request);

            request.Reply(data);
        }
    }
}