﻿using System.Linq;
using System.Threading.Tasks;
using Blaze.Common;
using Blaze.DataBridge;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Client.Inventory
{
    [Component(Component.Inventory)]
    [Command(1)]
    [ClientType(ClientType.GameplayUser)]
    public class GetItemsCommand : ICommandHandler
    {
        private readonly IDataBridge _dataBridge;
        private readonly ClientSessionManager _sessionManager;

        public GetItemsCommand(ClientSessionManager clientManager, IDataBridge dataBridge)
        {
            _sessionManager = clientManager;
            _dataBridge = dataBridge;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            // Lookup the session that was created during the pre-auth

            var category = packet.Content.GetInteger("CFLG");

            var inventory = new TdfStruct("INVT");
            if (category == 2)
            {
                var items = new TdfList<long>("BLST", TdfType.Integer);
                var allItemsUnlocked = Lists.Repeated((long)0,3243);
                items.Values = allItemsUnlocked;
                inventory.Values.Add(items);
                items.Recount();
            }
            else
            {
                var items = new TdfList<TdfStruct>("CLST", TdfType.Struct);
                inventory.Values.Add(items);
                items.Recount();
            }

            var data = new TdfContainer
            {
                inventory
            };

            var session = _sessionManager.Lookup(request);

            request.Reply(data);
        }
    }
}