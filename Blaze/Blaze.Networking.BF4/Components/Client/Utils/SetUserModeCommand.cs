﻿using System.Threading.Tasks;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking.BF4.Components.Client.Utils
{
    [Component(Component.Util)]
    [Command(28)]
    [ClientType(ClientType.GameplayUser)]
    public class SetUserModeCommand : ICommandHandler
    {
        private readonly ClientSessionManager _sessionManager;

        public SetUserModeCommand(ClientSessionManager clientManager)
        {
            _sessionManager = clientManager;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            var session = _sessionManager.Lookup(request);

            session.UserMode = packet.Content.GetInteger("MODE");

            request.Reply();
        }
    }
}