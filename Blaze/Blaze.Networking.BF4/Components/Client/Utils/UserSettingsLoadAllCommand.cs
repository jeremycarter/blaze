﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Client.Utils
{
    [Component(Component.Util)]
    [Command(12)]
    [ClientType(ClientType.GameplayUser)]
    public class UserSettingsLoadAllCommand : ICommandHandler
    {
        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            var data = new TdfContainer
            {
                new TdfMap("SMAP", TdfType.String, TdfType.String, new SortedDictionary<object, object>
                {
                    {"cust", "6YAJmA=="}
                })
            };

            request.Reply(data);
        }
    }
}