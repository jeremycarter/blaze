﻿using System.Threading.Tasks;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Client.Utils
{
    [Component(Component.Util)]
    [Command(2)]
    [ClientType(ClientType.GameplayUser)]
    public class PingCommand : ICommandHandler
    {
        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            var data = new TdfContainer
            {
                new TdfInteger("STIM", Utilities.GetUnixTime())
            };

            request.Reply(data);
        }
    }
}