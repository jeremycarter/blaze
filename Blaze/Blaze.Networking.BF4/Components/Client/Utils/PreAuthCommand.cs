﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Blaze.Common;
using Blaze.DataBridge;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Client.Utils
{
    [Component(Component.Util)]
    [Command(7)]
    [ClientType(ClientType.GameplayUser)]
    public class PreAuthCommand : ICommandHandler
    {
        private readonly IDataBridge _dataBridge;
        private readonly ClientSessionManager _sessionManager;

        public PreAuthCommand(ClientSessionManager clientManager, IDataBridge dataBridge)
        {
            _sessionManager = clientManager;
            _dataBridge = dataBridge;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            Log.Request($"Client {request.Client.EndPoint} is pre-authenticating");

            var clientData = packet.Content.GetItem<TdfStruct>("CDAT");
            var clientType = (ClientType) ((TdfInteger) clientData.Values.Find(tdf => tdf.Label == "TYPE")).Value;
            var clientService = (TdfString) clientData.Values.Find(tdf => tdf.Label == "SVCN");

            var clientInfo = packet.Content.GetItem<TdfStruct>("CINF");
            var clientLocalization = (TdfInteger) clientInfo.Values.Find(tdf => tdf.Label == "LOC");

            var session = _sessionManager.StartTracking(request.Client, clientType);
            session.Service = clientService.Value;
            session.Localization = clientLocalization.Value;

            request.Client.SessionId = session.Id;

            // TODO: fix this
            var cids = new TdfList<long>("CIDS", TdfType.Integer);
            var values = new long[]
            {1, 25, 4, 27, 28, 6, 7, 9, 10, 11, 30720, 30721, 30722, 30723, 20, 30725, 30726, 2000};
            foreach (var value in values)
            {
                cids.Values.Add(value);
            }

            var data = new TdfContainer
            {
                new TdfInteger("ANON", 0),
                new TdfString("ASRC", "300294"),
                cids,
                new TdfStruct("CONF", new List<ITdf>
                {
                    new TdfMap("CONF", TdfType.String, TdfType.String, new SortedDictionary<object, object>
                    {
                        {"associationListSkipInitialSet", "1"},
                        {"blazeServerClientId", "GOS-BlazeServer-BF4-PC"},
                        {"bytevaultHostname", "bytevault.gameservices.ea.com"},
                        {"bytevaultPort", "42210"},
                        {"bytevaultSecure", "false"},
                        {"capsStringValidationUri", "client-strings.xboxlive.com"},
                        {"connIdleTimeout", "90s"},
                        {"defaultRequestTimeout", "60s"},
                        {"identityDisplayUri", "console2/welcome"},
                        {"identityRedirectUri", "http://127.0.0.1/success"},
                        {"nucleusConnect", "http://127.0.0.1"},
                        {"nucleusProxy", "http://127.0.0.1/"},
                        {"pingPeriod", "20s"},
                        {"userManagerMaxCachedUsers", "0"},
                        {"voipHeadsetUpdateRate", "1000"},
                        {"xblTokenUrn", "http://127.0.0.1"},
                        {"xlspConnectionIdleTimeout", "300"}
                    })
                }),
                new TdfString("INST", "battlefield-4-pc"),
                new TdfInteger("MINR", false),
                new TdfString("NASP", "cem_ea_id"),
                new TdfString("PLAT", "pc"),
                new TdfStruct("QOSS", new List<ITdf>
                {
                    new TdfStruct("BWPS", new List<ITdf>
                    {
                        new TdfString("PSA", "localhost"),
                        new TdfInteger("PSP", 17502),
                        new TdfString("SNA", "rs-prod-lhr-bf4")
                    }),
                    new TdfInteger("LNP", 10),
                    new TdfMap("LTPS", TdfType.String, TdfType.Struct, new SortedDictionary<object, object>
                    {
                        {
                            "ams", new TdfStruct("VALU", new List<ITdf>
                            {
                                new TdfString("PSA", "gosprod-qos01.m3d-ams.ea.com"),
                                new TdfInteger("PSP", 17502),
                                new TdfString("SNA", "ea-prod-ams-bf3")
                            })
                         },
                         {
                            "dfw", new TdfStruct("VALU", new List<ITdf>
                            {
                                new TdfString("PSA", "gosprod-qos01.m3d-dfw.ea.com"),
                                new TdfInteger("PSP", 17502),
                                new TdfString("SNA", "mis-prod-dfw-bf3")
                            })
                        },
                        {
                            "iad", new TdfStruct("VALU", new List<ITdf>
                            {
                                new TdfString("PSA", "gosprod-qos01.m3d-iad.ea.com"),
                                new TdfInteger("PSP", 17502),
                                new TdfString("SNA", "ea-prod-iad-bf3")
                            })
                        },
                        {
                            "lax", new TdfStruct("VALU", new List<ITdf>
                            {
                                new TdfString("PSA", "gosprod-qos01.m3d-lax.ea.com"),
                                new TdfInteger("PSP", 17502),
                                new TdfString("SNA", "ea-prod-lax-bf3")
                            })
                        },
                        {
                            "lhr", new TdfStruct("VALU", new List<ITdf>
                            {
                                new TdfString("PSA", "gosprod-qos01.rspc-lhr.ea.com"),
                                new TdfInteger("PSP", 17502),
                                new TdfString("SNA", "rs-prod-lhr-bf3")
                            })
                        },
                        {
                            "nrt", new TdfStruct("VALU", new List<ITdf>
                            {
                                new TdfString("PSA", "gosprod-qos01.m3d-nrt.ea.com"),
                                new TdfInteger("PSP", 17502),
                                new TdfString("SNA", "i3d-prod-nrt-bf3")
                            })
                        },
                        {
                            "syd", new TdfStruct("VALU", new List<ITdf>
                            {
                                new TdfString("PSA", "gosprod-qos01.m3d-syd.ea.com"),
                                new TdfInteger("PSP", 17502),
                                new TdfString("SNA", "ea-prod-syd-bf3")
                            })
                        }
                    }),
                    new TdfInteger("SVID", 1337)
                }),
                new TdfString("RSRC", "302123"),
                new TdfString("SVER", "Blaze 13.15.08.0 (CL# 9442625)")
            };

            request.Reply(data);
        }
    }
}