﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Blaze.Common;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Client.Utils
{
    [Component(Component.Util)]
    [Command(8)]
    [ClientType(ClientType.GameplayUser)]
    public class PostAuthCommand : ICommandHandler
    {
        private readonly ClientSessionManager _clientManager;

        public PostAuthCommand(ClientSessionManager clientManager)
        {
            _clientManager = clientManager;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            Log.Request($"Client {request.Client.EndPoint} post-authenticating");

            var macAddress = packet.Content.GetString("MAC");

            var client = _clientManager.Lookup(request);

            var data = new TdfContainer
            {
                new TdfStruct("PSS", new List<ITdf>
                {
                    new TdfString("ADRS", ""),
                    new TdfInteger("AMAX", 0),
                    new TdfInteger("OMAX", 0),
                    new TdfString("PJID", ""),
                    new TdfInteger("PORT", 0),
                    new TdfInteger("RPRT", 0),
                    new TdfInteger("TIID", 0)
                }),
                new TdfStruct("TELE", new List<ITdf>
                {
                    new TdfString("ADRS", "gostelemetry.blaze3.ea.com"),
                    new TdfInteger("ANON", 0),
                    new TdfString("DISA", ""),
                    new TdfInteger("EDCT", 0),
                    new TdfString("FILT", "-GAME/COMM/EXPD"),
                    new TdfInteger("LOC", client.Localization),
                    new TdfInteger("MINR", 0),
                    new TdfString("NOOK", ""),
                    new TdfInteger("PORT", 9988),
                    new TdfInteger("SDLY", 15000),
                    new TdfString("SESS", "session_" + client.Id),
                    new TdfString("SKEY", "key_" + client.Id),
                    new TdfInteger("SPCT", 75),
                    new TdfString("STIM", "Default"),
                    new TdfString("SVNM", "telemetry-3-common")
                }),
               new TdfStruct("TICK", new List<ITdf>
                {
                    new TdfString("ADRS", "10.10.78.150"),
                    new TdfInteger("PORT", 8999),
                    new TdfString("SKEY",
                        $"{client.UserId},10.10.78.150:8999,battlefield-4-pc,10,50,50,50,50,0,0")
                }),
                new TdfStruct("UROP", new List<ITdf>
                {
                    new TdfInteger("TMOP", (ulong) TelemetryOpt.OptOut),
                    new TdfInteger("UID", client.UserId)
                })
            };

            request.Reply(data);
        }
    }
}