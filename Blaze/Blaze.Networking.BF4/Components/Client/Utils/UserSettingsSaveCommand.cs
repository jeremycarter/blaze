﻿using System.Threading.Tasks;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking.BF4.Components.Client.Utils
{
    [Component(Component.Util)]
    [Command(11)]
    [ClientType(ClientType.GameplayUser)]
    public class UserSettingsSaveCommand : ICommandHandler
    {
        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            request.Reply();
        }
    }
}