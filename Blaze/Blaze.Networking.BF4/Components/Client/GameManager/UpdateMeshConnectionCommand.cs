﻿using System.Threading.Tasks;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.BF4.Notifications.GameManager;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Client.GameManager
{
    [Component(Component.GameManager)]
    [Command(29)]
    [ClientType(ClientType.GameplayUser)]
    public class UpdateMeshConnectionCommand : ICommandHandler
    {
        private readonly GameSessionManager _gameManager;
        private readonly ClientSessionManager _sessionManager;

        public UpdateMeshConnectionCommand(GameSessionManager gameManager, ClientSessionManager sessionManager)
        {
            _gameManager = gameManager;
            _sessionManager = sessionManager;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            var gameId = packet.Content.GetInteger("GID");
            var stat = packet.Content.GetInteger("STAT");
            var tcg = packet.Content.GetItem<TdfTrippleValue>("TCG");

            var session = _sessionManager.Lookup(request);

            request.Reply();

            if (session != null)
            {
                // Status 2 is joining mesh, Status 1 is leaving mesh
                if (stat == 2)
                {
                    request.Notify(new GamePlayerStateChangeNotification(gameId, session.UserId));
                    request.Notify(new PlayerJoinCompletedNotification(gameId, session.UserId));
                }
                else
                {
                    request.Notify(new PlayerRemovedNotification(gameId, session.UserId));
                }
            }
        }
    }
}