﻿using System.Threading.Tasks;
using Blaze.Common;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.BF4.Notifications.GameManager;
using Blaze.Networking.BF4.Notifications.UserSession;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Client.GameManager
{
    [Component(Component.GameManager)]
    [Command(9)]
    [ClientType(ClientType.GameplayUser)]
    public class JoinGameCommand : ICommandHandler
    {
        private readonly GameSessionManager _gameManager;
        private readonly ClientSessionManager _sessionManager;

        public JoinGameCommand(GameSessionManager gameManager, ClientSessionManager sessionManager)
        {
            _gameManager = gameManager;
            _sessionManager = sessionManager;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            var session = _sessionManager.Lookup(request);
            var id = packet.Content.GetInteger("GID");

            Log.Request($"Client {session.Email} requesting to join gameid {id}");

            var slotType = SlotType.Soldier;
            if (packet.Content.ContainsKey("SLOT"))
            {
                slotType = (SlotType) packet.Content.GetInteger("SLOT");
            }
            else if (packet.Content.ContainsKey("APDC"))
            {
                var type = packet.Content.GetString("APDC");
                if (type == "soldier")
                {
                    slotType = SlotType.Soldier;
                }
            }

            session.Data["SlotType"] = slotType.ToString().ToLower();

            var game = _gameManager.Lookup(id);
            if (game == null)
            {
                // Error code, game does not exist
                request.ReplyError();
            }

            // The game is not null
            // ReSharper disable once PossibleNullReferenceException
            var serverClient = _sessionManager.LookupClient(game.ClientSessionId);
            if (serverClient != null && game.LastSeen > Time.ThreeHoursAgo())
            {
                var serverSession = _sessionManager.Lookup(game.ClientSessionId);

                long slotId = 0;
                if (game.Slots.Count < game.MaxPlayers)
                {
                    var slot = game.Slots.Add(session, slotType);
                    slotId = slot.Index;
                    // Reservation made, assume the client will continue to load the game
                    session.GameId = game.Id;
                }

                serverClient.Notify(new UserAddedNotification(session, 0, session.PersonaId));
                serverClient.Notify(new UserUpdatedNotification(session));
                serverClient.Notify(new PlayerJoiningNotification(session, game, slotId));

                var data = new TdfContainer
                {
                    new TdfInteger("GID", game.Id),
                    new TdfInteger("JGS", 0)
                };

                request.Reply(data);

                // Send the server session personaId
                data = new TdfContainer
                {
                    new TdfInteger("GID", game.Id),
                    new TdfInteger("PHID", serverSession.PersonaId),
                    new TdfInteger("PHST", 0)
                };
                request.Reply(71, data);

                request.Notify(new UserAddedNotification(serverSession, session.PersonaId, session.PersonaId));
                request.Notify(new UserUpdatedNotification(serverSession));

                request.Notify(new JoiningPlayerInitiateConnectionsNotification(session, serverSession, game, slotId));

                serverClient.Notify(new PlayerClaimingReservationNotification(session, game, slotId));

                serverClient.Notify(new UserNetworkInfoUpdatedNotification(session));

                Log.Info($"Client {session.Email} is joining gameid {id}");
            }
            else
            {
                // Error code, game does not exist
                request.ReplyError();
            }
        }
    }
}