﻿using System;
using System.Net;
using System.Threading.Tasks;
using Blaze.Common.Extensions;
using Blaze.DataBridge;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.BF4.Notifications.UserSession;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Client.UserSession
{
    [Component(Component.UserSessions)]
    [Command(20)]
    [ClientType(ClientType.GameplayUser)]
    public class UpdateNetworkInfoCommand : ICommandHandler
    {
        private readonly IDataBridge _dataBridge;
        private readonly ClientSessionManager _sessionManager;

        public UpdateNetworkInfoCommand(ClientSessionManager clientManager, IDataBridge dataBridge)
        {
            _sessionManager = clientManager;
            _dataBridge = dataBridge;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            var session = _sessionManager.Lookup(request);
            if (session != null)
            {
                var addr = packet.Content.GetItem<TdfUnion>("ADDR");
                var valu = (TdfStruct) addr.Content;

                var internalIp = session.InternalEndpoint.Address.ToLong();
                var internalPort = session.InternalEndpoint.Port;
                var externalIp = session.ExternalEndpoint.Address.ToLong();
                var externalPort = session.ExternalEndpoint.Port;

                if (valu != null)
                {
                    var inip = (TdfStruct) valu.Values.Find(tdf => tdf.Label == "INIP");
                    var ip = (TdfInteger) inip.Values.Find(tdf => tdf.Label == "IP");
                    var port = (TdfInteger) inip.Values.Find(tdf => tdf.Label == "PORT");
                    internalIp = ip.Value;
                    internalPort = Convert.ToInt32(port.Value);
                }

                session.InternalEndpoint = new IPEndPoint(internalIp.ToIPAddress(), internalPort);
                session.ExternalEndpoint = new IPEndPoint(internalIp.ToIPAddress(), internalPort);
            }

            request.ReplyEmpty();

            if (session != null && session.ClientType == ClientType.GameplayUser)
            {
                if (session.GameId == 0)
                {
                    request.Notify(new UserNetworkInfoUpdatedNotification(session));
                }
                else
                {
                    request.Notify(new UserNetworkInfoUpdatedServerNotification(session));
                }
                
            }
        }
    }
}