﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Blaze.Common;
using Blaze.DataBridge;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.BF4.Notifications.UserSession;
using Blaze.Networking.Clients;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Components.Client.Accounts
{
    [Component((ushort)Component.Accounts)]
    [Command(10)]
    public class GetUserSessionFromLoginCommand : ICommandHandler
    {
        private readonly IDataBridge _dataBridge;
        private readonly ClientSessionManager _sessionManager;

        public GetUserSessionFromLoginCommand(ClientSessionManager clientManager, IDataBridge dataBridge)
        {
            _sessionManager = clientManager;
            _dataBridge = dataBridge;
        }

        public void Handle(BlazeClientRequest request, BlazePacket packet)
        {
            // Lookup the session that was created during the pre-auth

            var session = _sessionManager.Lookup(request);

            var token = packet.Content.GetString("AUTH");

            Log.Request($"Client {request.Client.EndPoint} logging in with value {token}");

            Log.Warn("User not found - using dummy user");
            session.UserId = 955;
            session.PersonaId = session.UserId;
            session.Email = token;
            session.Name = "Player1";
            session.PasswordHash = Hasher.SHA1(token);

            // Update the player ticket to represent their userid
            session.Ticket = $"PlayerTicket_{session.UserId}";

            var data = new TdfContainer
            {
                new TdfInteger("ANON", false),
                new TdfInteger("NTOS", false),
                new TdfStruct("SESS", new List<ITdf>
                {
                    new TdfInteger("BUID", session.UserId),
                    new TdfInteger("FRSC", false),
                    new TdfInteger("FRST", false),
                    new TdfString("KEY", session.Key),
                    new TdfInteger("LLOG", 1403663841),
                    new TdfString("MAIL", session.Email),
                    new TdfStruct("PDTL", new List<ITdf>
                    {
                        new TdfString("DSNM", session.Name),
                        new TdfInteger("PID", session.UserId),
                        new TdfInteger("PLAT", (ulong) Platform.PC)
                    }),
                    new TdfInteger("UID", session.Id),
                }),
                new TdfInteger("SPAM", true),
                new TdfInteger("UNDR", false)
            };

            request.Reply(data);

            request.Notify(new SessionCreatedNotification(session));
            request.Notify(new UserAddedNotification(session, 0, session.PersonaId));
            request.Notify(new UserUpdatedNotification(session));
        }
    }
}