﻿namespace Blaze.Networking.BF4
{
    public enum Status : ushort
    {
        Inactive = 1,
        Active = 2
    }

    public enum SlotType : ushort
    {
        Any = 0,
        Soldier = 1,
        Spectator = 2,
        Commander = 3
    }

    public enum Platform : ushort
    {
        Unknown,
        XboxLive,
        Playstation,
        Wii,
        PC
    }

    public enum Component : ushort
    {
        Packs = 2050,
        Inventory = 2051, // x0803
        Friends = 25,
        Accounts = 35,
        Authentication = 1,
        GameManager = 4,
        Redirector = 5,
        Stats = 7,
        Util = 9,
        Clubs = 11,
        GameReporting = 0x1C,
        Rsp = 0x801,
        UserSessions = 0x7802,
        Unknown = 2049
    }

    public enum NetworkAddressMember : ushort
    {
        XboxClientAddress,
        XboxServerAddress,
        IppAirAddress,
        IpAddress,
        HostnameAddress,
        Unset = 0x7F
    }

    public enum UpnpStatus : ushort
    {
        Unknown,
        Found,
        Enabled
    }

    public enum TelemetryOpt : ushort
    {
        OptOut,
        OptIn
    }

    public enum GameState : ushort
    {
        NewState,
        Initializing,
        Virtual,
        PreGame = 130,
        InGame = 131,
        PostGame = 4,
        Migrating,
        Destructing,
        Resetable,
        ReplaySetup
    }

    public enum PlayerState : ushort
    {
        Disconnected,
        Connected = 2
    }

    public enum PresenceMode : ushort
    {
        None,
        Standard,
        Private
    }

    public enum VoipTopology : ushort
    {
        Disabled,
        DedicatedServer,
        PeerToPeer
    }

    public enum GameNetworkTopology : ushort
    {
        ClientServerPeerHosted,
        ClientServerDedicated,
        PeerToPeerFullMesh = 0x82,
        PeerToPeerPartialMesh,
        PeerToPeerDirtyCastFailover
    }

    public enum PlayerRemovedReason : ushort
    {
        PlayerJoinTimeout,
        PlayerConnLost,
        BlazeServerConnLost,
        MigrationFailed,
        GameDestroyed,
        GameEnded,
        PlayerLeft,
        GroupLeft,
        PlayerKicked,
        PlayerKickedWithBan,
        PlayerJoinFromQueueFailed,
        PlayerReservationTimeout,
        HostEjected
    }

    public enum ExternalRefType : ushort
    {
        Unknown,
        Xbox,
        Ps3,
        Wii,
        Mobile,
        LegacyProfileId,
        Twitter,
        Facebook
    }

    public enum NatType : ushort
    {
        Open,
        Moderate,
        Sequential,
        Strict,
        Unknown
    }
}