﻿using System.Collections.Generic;
using Blaze.Common;
using Blaze.Common.Extensions;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Components;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Notifications.GameManager
{
    public class JoiningPlayerInitiateConnectionsNotification : INotification
    {
        private readonly ClientSession _client;
        private readonly ClientSession _serverSession;
        private readonly GameSession _game;
        private readonly long _slotId;

        public JoiningPlayerInitiateConnectionsNotification(ClientSession client, ClientSession serverSession, GameSession game, long slotId)
        {
            _client = client;
            _serverSession = serverSession;
            _game = game;
            _slotId = slotId;
            Component = (ushort) BF4.Component.GameManager;
            Command = 0x16;
        }

        public ushort Component { get; set; }
        public ushort Command { get; set; }

        public TdfContainer Container()
        {
            Log.Info($"Player {_client.UserId} is initiating communications with server {_game.Id}");

            var slotType = "soldier";
            if (_client.Data.ContainsKey("SlotType"))
            {
                slotType = (string) _client.Data["SlotType"];
            }

            var hnet = new TdfList<TdfStruct>("HNET", TdfType.Struct,
                new List<TdfStruct>
                {
                    new TdfStruct("VALU", new List<ITdf>
                    {
                        new TdfStruct("EXIP", new List<ITdf>
                        {
                            new TdfInteger("IP", _game.ExternalEndpoint.Address.ToLong()),
                            new TdfInteger("PORT", _game.ExternalEndpoint.Port)
                        }),
                        new TdfStruct("INIP", new List<ITdf>
                        {
                            new TdfInteger("IP", _game.InternalEndpoint.Address.ToLong()),
                            new TdfInteger("PORT", _game.InternalEndpoint.Port)
                        })
                    }, true)
                });
            var data = new TdfContainer
            {
                new TdfStruct("GAME", new List<ITdf>
                {
                    new TdfMap("ATTR", TdfType.String, TdfType.String, _game.Attributes),
                    new TdfList<long>("CAP", TdfType.Integer, _game.Capacity),
                    new TdfInteger("GID", _game.Id),
                    new TdfInteger("GMRG", _game.ManagerId),
                    new TdfString("GNAM", _game.Name),
                    new TdfInteger("GPVH", 6667),
                    new TdfInteger("GSET", _game.Settings),
                    new TdfInteger("GSID", 1337),
                    new TdfInteger("GSTA", (ulong) _game.State),
                    new TdfString("GTYP", "frostbite_multiplayer"),
                    hnet,
                    new TdfInteger("HSES", 13666),
                    new TdfInteger("IGNO", 0),
                    new TdfInteger("MCAP", _game.MaxPlayers),
                    new TdfInteger("MNCP", 1),
                    new TdfInteger("NRES", _game.NotResettable),
                    new TdfInteger("NTOP", (ulong) _game.NetworkTopology),
                    new TdfString("PGID", _game.Id.ToString()),
                    new TdfStruct("PHST", new List<ITdf>
                    {
                        new TdfInteger("CSID", _slotId),
                        new TdfInteger("HPID", _client.PersonaId),
                        new TdfInteger("HSLT", _slotId)
                    }),
                    new TdfInteger("PRES", (ulong) _game.PresenceMode),
                    new TdfString("PSAS", "ams"),
                    new TdfInteger("QCAP", _game.QueueCapacity),
                    new TdfHex(
                        "cae9af038f2a74050103020a636f6d6d616e646572008f2a74050101010e636f6d6d616e64657252616e6b001173746174735f72616e6b203e3d20313000ca387000020008736f6c6469657200ca3870001e0000"),
                    new TdfInteger("SEED", 11181),
                    new TdfStruct("THST", new List<ITdf>
                    {
                        new TdfInteger("CSID", 0),
                        new TdfInteger("HPID", _serverSession.PersonaId),
                        new TdfInteger("HSLT", 0)
                    }),
                    new TdfIntegerList("TIDS", new List<long> {65534}),
                    new TdfString("UUID", _game.Guid.ToString()),
                    new TdfInteger("VOIP", (ulong) _game.VoipTopology),
                    new TdfString("VSTR", "4900"),
                    new TdfBinary("XNNC", null),
                    new TdfBinary("XSES", null)
                }),
                new TdfInteger("LFPJ", 0),
                new TdfList<ITdf>("PROS", TdfType.Struct,
                    new List<ITdf>
                    {
                        new TdfBinary("BLOB", new byte[] {}),
                        new TdfInteger("CSID", _slotId),
                        new TdfInteger("EXID", 0),
                        new TdfInteger("GID", _client.GameId),
                        new TdfInteger("JFPS", 0),
                        new TdfInteger("LOC", _client.Localization),
                        new TdfString("NAME", _client.Name),
                        new TdfMap("PATT", TdfType.String, TdfType.String, new SortedDictionary<object, object>
                        {
                            {"Premium", "False"}
                        }),
                        new TdfInteger("PID", _client.UserId),
                        new TdfUnion("PNET", (ushort) NetworkAddressMember.IppAirAddress,
                            new TdfStruct("VALU", new List<ITdf>
                            {
                                new TdfStruct("EXIP", new List<ITdf>
                                {
                                    new TdfInteger("IP", _client.ExternalEndpoint.Address.ToLong()),
                                    new TdfInteger("PORT", _client.ExternalEndpoint.Port)
                                }),
                                new TdfStruct("INIP", new List<ITdf>
                                {
                                    new TdfInteger("IP", _client.InternalEndpoint.Address.ToLong()),
                                    new TdfInteger("PORT", _client.InternalEndpoint.Port)
                                })
                            }, false)
                            ),
                        new TdfString("ROLE", slotType),
                        new TdfInteger("SID", _slotId),
                        new TdfInteger("SLOT", 0),
                        new TdfInteger("STAT", 2),
                        new TdfInteger("TIDX", 0),
                        new TdfInteger("TIME", 0),
                        new TdfInteger("UID", _client.UserId),
                        new TdfInteger("REAS", 0),
                    })
            };

            return data;
        }
    }
}