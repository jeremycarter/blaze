﻿using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Components;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Notifications.GameManager
{
    public class GameFinalizedNotification : INotification
    {
        private readonly GameSession _game;
        private readonly ClientSession _session;

        public GameFinalizedNotification(GameSession game, ClientSession session)
        {
            _game = game;
            _session = session;
            Component = (ushort) BF4.Component.GameManager;
            Command = 0x47;
        }

        public ushort Component { get; set; }
        public ushort Command { get; set; }

        public TdfContainer Container()
        {
            var data = new TdfContainer
            {
                new TdfInteger("GID", _game.Id),
                new TdfInteger("PHID", _session.UserId),
                new TdfInteger("PHST", 0)
            };
            return data;
        }
    }
}