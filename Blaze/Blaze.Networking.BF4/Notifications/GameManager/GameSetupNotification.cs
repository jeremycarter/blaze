﻿using System.Collections.Generic;
using System.Linq;
using Blaze.Common.Extensions;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Components;
using Blaze.Protocol;
using Blaze.Protocol.Types;
using ByteExtensions = Blaze.Common.Extensions.ByteExtensions;

namespace Blaze.Networking.BF4.Notifications.GameManager
{
    public class GameSetupNotification : INotification
    {
        private readonly ClientSession _client;
        private readonly GameSession _game;

        public GameSetupNotification(ClientSession client, GameSession game)
        {
            _client = client;
            _game = game;
            Component = (ushort) BF4.Component.GameManager;
            Command = 20;
        }

        public ushort Component { get; set; }
        public ushort Command { get; set; }

        public TdfContainer Container()
        {
            var hnet = new TdfList<TdfStruct>("HNET", TdfType.Struct,
                new List<TdfStruct>
                {
                    new TdfStruct("VALU", new List<ITdf>
                    {
                        new TdfStruct("EXIP", new List<ITdf>
                        {
                            new TdfInteger("IP", _game.ExternalEndpoint.Address.ToLong()),
                            new TdfInteger("PORT", _game.ExternalEndpoint.Port)
                        }),
                        new TdfStruct("INIP", new List<ITdf>
                        {
                            new TdfInteger("IP", _game.InternalEndpoint.Address.ToLong()),
                            new TdfInteger("PORT", _game.InternalEndpoint.Port)
                        })
                    }, true)
                });

            var pgsr =
                "741aa5db00da730dab837a88f2088faef8662f8b43e09a4c34d7421e4d977e19958d55b9d025408f4082acd6f70d6690ffeb7fe437c7f905";
            var data = new TdfContainer
            {
                new TdfStruct("GAME", new TdfContainer
                {
                    new TdfList<long>("ADMN", TdfType.Integer, new List<long>
                    {
                        _client.UserId
                    }),
                    new TdfMap("ATTR", TdfType.String, TdfType.String, _game.Attributes),
                    new TdfList<long>("CAP", TdfType.Integer, _game.Capacity.Select(i => i)),
                    new TdfString("COID", ""),
                    new TdfString("ESNM", ""),
                    new TdfInteger("GID", _game.Id),
                    new TdfInteger("GMRG", _game.ManagerId),
                    new TdfString("GNAM", _game.Name),
                    new TdfInteger("GPVH", 6667),
                    new TdfInteger("GSET", _game.Settings),
                    new TdfInteger("GSID", 1337),
                    new TdfInteger("GSTA", (long) _game.State),
                    new TdfString("GTYP", "frostbite_multiplayer"),
                    new TdfString("GURL", ""),
                    hnet,
                    new TdfInteger("HSES", 13666),
                    new TdfInteger("IGNO", false),
                    new TdfInteger("MCAP", _game.MaxPlayers),
                    new TdfStruct("NQOS", new List<ITdf>
                    {
                        new TdfInteger("DBPS", 0),
                        new TdfInteger("NATT", 4),
                        new TdfInteger("UBPS", 0)
                    }, false),
                    new TdfInteger("NRES", _game.NotResettable),
                    new TdfInteger("NTOP", (long) _game.NetworkTopology),
                    new TdfString("PGID", _game.Guid.ToString()),
                    new TdfBinary("PGSR", ByteExtensions.ToByteArray(pgsr)),
                    new TdfStruct("PHST", new List<ITdf>
                    {
                        new TdfInteger("HPID", _client.UserId),
                        new TdfInteger("HSLT", 1)
                    }, false),
                    new TdfInteger("PRES", (long) _game.PresenceMode),
                    new TdfString("PSAS", "ams"),
                    new TdfInteger("QCAP", _game.QueueCapacity),
                    new TdfUnion("REAS", 0, null),
                    new TdfStruct("VALU", new List<ITdf>
                    {
                        new TdfInteger("DCTX", 0)
                    }),
                    new TdfInteger("SEED", 11181),
                    new TdfInteger("TCAP", 0),
                    new TdfStruct("THST", new List<ITdf>
                    {
                        new TdfInteger("HPID", _game.Id),
                        new TdfInteger("HSLT", 0)
                    }),
                    new TdfString("UUID", _game.Guid.ToString()),
                    new TdfInteger("VOIP", (long) _game.VoipTopology),
                    new TdfString("VSTR", "4900")
                })
            };

            return data;
        }
    }
}