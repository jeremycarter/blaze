﻿using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Components;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Notifications.GameManager
{
    public class GameSettingsChangeNotification : INotification
    {
        private readonly GameSession _game;

        public GameSettingsChangeNotification(GameSession game)
        {
            _game = game;
            Component = (ushort) BF4.Component.GameManager;
            Command = 0x6E;
        }

        public ushort Component { get; set; }
        public ushort Command { get; set; }

        public TdfContainer Container()
        {
            var data = new TdfContainer
            {
                new TdfInteger("ATTR", _game.Settings),
                new TdfInteger("GID", _game.Id)
            };

            return data;
        }
    }
}