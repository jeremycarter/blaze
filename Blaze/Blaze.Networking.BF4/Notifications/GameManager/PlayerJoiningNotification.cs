﻿using System.Collections.Generic;
using Blaze.Common.Extensions;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Components;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Notifications.GameManager
{
    public class PlayerJoiningNotification : INotification
    {
        private readonly ClientSession _client;
        private readonly GameSession _game;
        private readonly long _slotId;

        public PlayerJoiningNotification(ClientSession client, GameSession game, long slotId)
        {
            _client = client;
            _game = game;
            _slotId = slotId;
            Component = (ushort) BF4.Component.GameManager;
            Command = 21;
        }

        public ushort Component { get; set; }
        public ushort Command { get; set; }

        public TdfContainer Container()
        {
            var slotType = "soldier";
            if (_client.Data.ContainsKey("SlotType"))
            {
                slotType = (string) _client.Data["SlotType"];
            }

            var data = new TdfContainer
            {
                new TdfInteger("GID", _client.GameId),
                new TdfStruct("PDAT", new List<ITdf>
                {
                    new TdfInteger("CONG", _client.UserId),
                    new TdfInteger("CSID", _slotId),
                    new TdfInteger("EXID", 0),
                    new TdfInteger("GID", _client.GameId),
                    new TdfInteger("LOC", _client.Localization),
                    new TdfString("NAME", _client.Name),
                    new TdfMap("PATT", TdfType.String, TdfType.String, new SortedDictionary<object, object>
                    {
                        {"Premium", "False"}
                    }),
                    new TdfInteger("PID", _client.UserId),
                    new TdfUnion("PNET", (ushort) NetworkAddressMember.IppAirAddress,
                        new TdfStruct("VALU", new List<ITdf>
                        {
                            new TdfStruct("EXIP", new List<ITdf>
                            {
                                new TdfInteger("IP", _client.ExternalEndpoint.Address.ToLong()),
                                new TdfInteger("PORT", _client.ExternalEndpoint.Port)
                            }),
                            new TdfStruct("INIP", new List<ITdf>
                            {
                                new TdfInteger("IP", _client.InternalEndpoint.Address.ToLong()),
                                new TdfInteger("PORT", _client.InternalEndpoint.Port)
                            })
                        }, false)
                        ),
                    new TdfString("ROLE", slotType),
                    new TdfInteger("SID", _slotId),
                    new TdfInteger("SLOT", 0),
                    new TdfInteger("STAT", 0),
                    new TdfInteger("TIDX", 0),
                    new TdfInteger("TIME", 0),
                    new TdfInteger("UID", _client.UserId)
                })
            };
            return data;
        }
    }
}