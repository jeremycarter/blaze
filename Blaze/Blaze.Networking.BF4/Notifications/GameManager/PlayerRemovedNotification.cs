﻿using Blaze.Networking.Components;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Notifications.GameManager
{
    public class PlayerRemovedNotification : INotification
    {
        private readonly long _gameId;
        private readonly long _playerId;

        public PlayerRemovedNotification(long gameId, long playerId)
        {
            _gameId = gameId;
            _playerId = playerId;
            Component = (ushort) BF4.Component.GameManager;
            Command = 40;
        }

        public ushort Component { get; set; }
        public ushort Command { get; set; }

        public TdfContainer Container()
        {
            var data = new TdfContainer
            {
                new TdfInteger("GID", _gameId),
                new TdfInteger("PID", _playerId),
                new TdfInteger("REAS", 1)
            };

            return data;
        }
    }
}