﻿using Blaze.Networking.Components;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Notifications.GameManager
{
    public class GameModChangedNotification : INotification
    {
        private readonly long _gameId;
        private readonly long _managerId;

        public GameModChangedNotification(long gameId, long managerId)
        {
            _gameId = gameId;
            _managerId = managerId;
            Component = (ushort) BF4.Component.GameManager;
            Command = 123;
        }

        public ushort Component { get; set; }
        public ushort Command { get; set; }

        public TdfContainer Container()
        {
            var data = new TdfContainer
            {
                new TdfInteger("GMID", _gameId),
                new TdfInteger("GMRG", _managerId)
            };

            return data;
        }
    }
}