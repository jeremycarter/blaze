﻿using System.Collections.Generic;
using Blaze.Common.Extensions;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Components;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Notifications.UserSession
{
    public class UserNetworkInfoUpdatedNotification : INotification
    {
        private readonly ClientSession _session;

        public UserNetworkInfoUpdatedNotification(ClientSession session)
        {
            _session = session;
            Component = (ushort) BF4.Component.UserSessions;
            Command = 1;
        }

        public ushort Component { get; set; }
        public ushort Command { get; set; }

        public TdfContainer Container()
        {
            var data = new TdfContainer
            {
                new TdfStruct("DATA", new List<ITdf>
                {
                    new TdfUnion("ADDR", (ushort) NetworkAddressMember.IppAirAddress,
                        new TdfStruct("VALU", new List<ITdf>
                        {
                            new TdfStruct("EXIP", new List<ITdf>
                            {
                                new TdfInteger("IP", _session.ExternalEndpoint.Address.ToLong()),
                                new TdfInteger("PORT", _session.ExternalEndpoint.Port)
                            }),
                            new TdfStruct("INIP", new List<ITdf>
                            {
                                new TdfInteger("IP", _session.InternalEndpoint.Address.ToLong()),
                                new TdfInteger("PORT", _session.InternalEndpoint.Port)
                            })
                        }, false)
                        )
                }),
                new TdfInteger("USID", _session.UserId)
            };

            return data;
        }
    }
}