﻿using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Components;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Notifications.UserSession
{
    public class UserUpdatedNotification : INotification
    {
        private readonly ClientSession _session;

        public UserUpdatedNotification(ClientSession session)
        {
            _session = session;
            Component = (ushort) BF4.Component.UserSessions;
            Command = 5;
        }

        public ushort Component { get; set; }
        public ushort Command { get; set; }

        public TdfContainer Container()
        {
            var data = new TdfContainer
            {
                new TdfInteger("FLGS", 3),
                new TdfInteger("ID", _session.PersonaId)
            };
            return data;
        }
    }
}