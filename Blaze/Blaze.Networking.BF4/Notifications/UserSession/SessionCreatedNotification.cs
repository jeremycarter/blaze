﻿using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Components;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Notifications.UserSession
{
    public class SessionCreatedNotification : INotification
    {
        private readonly ClientSession _session;

        public SessionCreatedNotification(ClientSession session)
        {
            _session = session;
            Component = (ushort) BF4.Component.UserSessions;
            Command = 8;
        }

        public ushort Component { get; set; }
        public ushort Command { get; set; }

        public TdfContainer Container()
        {
            TdfContainer data;

            if (_session.ClientType == ClientType.GameplayUser)
            {
                data = new TdfContainer
                {
                    new TdfInteger("ALOC", _session.Localization),
                    new TdfInteger("BUID", _session.UserId),
                    new TdfTrippleValue("CGID", 30722, 2, 2287828610704275869),
                    new TdfString("DSNM", _session.Name),
                    new TdfInteger("FRSC", 0),
                    new TdfInteger("FRST", false),
                    new TdfString("KEY", _session.Key),
                    new TdfInteger("LAST", 1403663841),
                    new TdfInteger("LLOG", 1403663841),
                    new TdfString("MAIL", _session.Email),
                    new TdfInteger("PID", _session.UserId),
                    new TdfInteger("PLAT", (long) Platform.PC),
                    new TdfInteger("UID", _session.UserId),
                    new TdfInteger("USTP", false),
                    new TdfInteger("XREF", 0)
                };
            }
            else
            {
                data = new TdfContainer
                {
                    new TdfInteger("ALOC", _session.Localization),
                    new TdfInteger("BUID", _session.UserId),
                    new TdfString("DSNM", _session.Name),
                    new TdfInteger("FRST", false),
                    new TdfString("KEY", _session.Key),
                    new TdfInteger("LAST", 1403663841),
                    new TdfInteger("LLOG", 1403663841),
                    new TdfString("MAIL", _session.Email),
                    new TdfInteger("PID", _session.UserId),
                    new TdfInteger("PLAT", (long) Platform.PC),
                    new TdfInteger("UID", _session.UserId),
                    new TdfInteger("USTP", false),
                    new TdfInteger("XREF", 0)
                };
            }

            return data;
        }
    }
}