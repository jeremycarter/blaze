﻿using System.Collections.Generic;
using Blaze.Common.Extensions;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Components;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Notifications.UserSession
{
    public class UserNetworkInfoUpdatedServerNotification : INotification
    {
        private readonly ClientSession _session;

        public UserNetworkInfoUpdatedServerNotification(ClientSession session)
        {
            _session = session;
            Component = (ushort) BF4.Component.UserSessions;
            Command = 1;
        }

        public ushort Component { get; set; }
        public ushort Command { get; set; }

        public TdfContainer Container()
        {
            var data = new TdfContainer
            {
                new TdfStruct("DATA", new List<ITdf>
                {
                    new TdfUnion("ADDR", (ushort) NetworkAddressMember.IppAirAddress,
                        new TdfStruct("VALU", new List<ITdf>
                        {
                            new TdfStruct("EXIP", new List<ITdf>
                            {
                                new TdfInteger("IP", _session.ExternalEndpoint.Address.ToLong()),
                                new TdfInteger("PORT", _session.ExternalEndpoint.Port)
                            }),
                            new TdfStruct("INIP", new List<ITdf>
                            {
                                new TdfInteger("IP", _session.InternalEndpoint.Address.ToLong()),
                                new TdfInteger("PORT", _session.InternalEndpoint.Port)
                            })
                        }, false)
                        ),
                    new TdfString("BPS", "ams"),
                    new TdfString("CTY", ""),
                    new TdfIntegerList("CVAR", null),
                    new TdfMap("DMAP", TdfType.Integer, TdfType.Integer, new SortedDictionary<object, object>
                    {
                        {458753, 43},
                        {458754, 397}
                    }),
                    new TdfInteger("HWFG", 0),
                    new TdfList<long>("PSLM", TdfType.Integer, new List<long>
                    {
                        268374015,
                        268374015,
                        268374015,
                        268374015,
                        268374015,
                        268374015
                    }),
                    new TdfStruct("QDAT", new List<ITdf>
                    {
                        new TdfInteger("DBPS", 0),
                        new TdfInteger("NATT", (long) NatType.Unknown),
                        new TdfInteger("UBPS", 0)
                    }),
                    new TdfInteger("UATT", 1513170665472),
                    new TdfTrippleValue("ULST", 2, 1, _session.UserId)
                }),
                new TdfInteger("USID", _session.UserId)
            };

            return data;
        }
    }
}