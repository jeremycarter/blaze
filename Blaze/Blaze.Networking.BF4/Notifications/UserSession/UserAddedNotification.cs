﻿using System.Collections.Generic;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Components;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.BF4.Notifications.UserSession
{
    public class UserAddedNotification : INotification
    {
        private readonly ClientSession _session;
        private readonly long _original;
        private readonly long _personId;

        public UserAddedNotification(ClientSession session, long original, long personId)
        {
            _session = session;
            _original = original;
            _personId = personId;
            Component = (ushort) BF4.Component.UserSessions;
            Command = 2;
        }

        public ushort Component { get; set; }
        public ushort Command { get; set; }

        public TdfContainer Container()
        {
            var dataStruct = new TdfStruct("DATA", new List<ITdf>
            {
                new TdfUnion("ADDR", (ushort) NetworkAddressMember.Unset, null),
                new TdfString("BPS", ""),
                new TdfString("CTY", ""),
                new TdfIntegerList("CVAR", null),
                new TdfInteger("HWFG", 0),
                new TdfStruct("QDAT", new List<ITdf>
                {
                    new TdfInteger("DBPS", 0),
                    new TdfInteger("NATT", (long) NatType.Open),
                    new TdfInteger("UBPS", 0)
                }),
                new TdfInteger("UATT", 0),
            });

            var data = new TdfContainer
            {
                dataStruct,
                new TdfStruct("USER", new List<ITdf>
                {
                    new TdfInteger("AID", _session.UserId),
                    new TdfInteger("ALOC", _session.Localization),
                    new TdfInteger("ID", _session.PersonaId),
                    new TdfString("NAME", _session.Name),
                    new TdfInteger("ORIG", _original),
                    new TdfInteger("PIDI", _personId)
                })
            };

            return data;
        }
    }
}