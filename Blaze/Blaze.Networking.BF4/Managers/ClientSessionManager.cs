﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using Blaze.Networking.ServerBase;

namespace Blaze.Networking.BF4.Managers
{
    public class ClientSessionManager
    {
        private long _sessionId;

        public ClientSessionManager()
        {
            Sessions = new HashSet<ClientSession>();
        }

        public HashSet<ClientSession> Sessions { get; set; }

        public long CreateId()
        {
            var id = Interlocked.Increment(ref _sessionId);
            return Convert.ToInt64(id);
        }

        public ClientSession StartTracking(ISecureTcpClient tcpClient, ClientType clientType = ClientType.GameplayUser)
        {
            tcpClient.SessionId = CreateId();
            return StartTracking(tcpClient, tcpClient.EndPoint, clientType);
        }

        public ClientSession StartTracking(ISecureTcpClient tcpClient, IPEndPoint ipAddress,
            ClientType clientType = ClientType.GameplayUser)
        {
            var lookup = Lookup(tcpClient.SessionId.Value);
            if (lookup == null)
            {
                var info = new ClientSession
                {
                    TcpClient = tcpClient,
                    Key = $"SessionKey_{tcpClient.SessionId}",
                    ClientType = clientType,
                    LastSeen = DateTime.Now,
                    InternalEndpoint = ipAddress,
                    ExternalEndpoint = ipAddress,
                    Id = tcpClient.SessionId.Value,
                    Data = new Dictionary<string, object>()
                };
                AddOrUpdate(info);
                return info;
            }
            return lookup;
        }

        public ClientSession AddOrUpdate(ClientSession information)
        {
            var lookup = Lookup(information.Id);
            if (lookup != null)
            {
                lookup.LastSeen = DateTime.Now;
                lookup.ClientType = information.ClientType;
                lookup.IpAddress = information.IpAddress;
                return lookup;
            }
            information.LastSeen = DateTime.Now;
            lock (Sessions)
            {
                Sessions.Add(information);
            }
            return information;
        }

        public ClientSession Lookup(ISecureTcpClient client)
        {
            if (client.SessionId == null) return null;
            return Lookup(client.SessionId.Value);
        }

        public ClientSession Lookup(BlazeClientRequest request)
        {
            if (request.Client.SessionId == null) return null;
            return Lookup(request.Client.SessionId.Value);
        }

        public ISecureTcpClient LookupClient(long id)
        {
            return Sessions.Where(i => i.Id == id).OrderByDescending(i => i.LastSeen).FirstOrDefault()?.TcpClient;
        }

        public ClientSession Lookup(long id)
        {
            return Sessions.Where(i => i.Id == id).OrderByDescending(i => i.LastSeen).FirstOrDefault();
        }

        public ClientSession LookupByKey(string key)
        {
            return Sessions.Where(i => i.Key == key).OrderByDescending(i => i.LastSeen).FirstOrDefault();
        }

        public ClientSession LookupByUserId(long userId)
        {
            return Sessions.Where(i => i.UserId == userId).OrderByDescending(i => i.LastSeen).FirstOrDefault();
        }

        public bool Update(ClientSession info, Action<ClientSession> action)
        {
            info.LastSeen = DateTime.Now;
            return Update(info.Id, action);
        }

        public bool Update(long id, Action<ClientSession> action)
        {
            var existing = Lookup(id);
            if (existing != null)
            {
                lock (Sessions)
                {
                    action.Invoke(existing);
                }
                return true;
            }
            return false;
        }

        public bool Remove(long id)
        {
            lock (Sessions)
            {
                Sessions.RemoveWhere(i => i.Id == id);
            }
            return true;
        }
    }
}