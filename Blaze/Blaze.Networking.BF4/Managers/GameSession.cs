﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Blaze.Networking.BF4.Managers
{
    public class GameSession : ISessionObject
    {
        public GameSession()
        {
            Guid = Guid.NewGuid();
            LastSeen = DateTime.Now;
            Data = new Dictionary<string, object>();
            Slots = new GameSlotContainer();
        }

        public long ClientSessionId { get; set; }
        public Guid Guid { get; set; }

        public string Country { get; set; }
        public string Name { get; set; }
        public string Level { get; set; }
        public string LevelLocation { get; set; }
        public string Hash { get; set; }
        public string Mode { get; set; }
        public string Preset { get; set; }
        public bool PunkBuster { get; set; }
        public bool Fairfight { get; set; }
        public bool NotResettable { get; set; }
        public string Region { get; set; }
        public string Type { get; set; }
        public int TickRate { get; set; }
        public List<long> Capacity { get; set; }
        public int MaxPlayers { get; set; }
        public int QueueCapacity { get; set; }
        public PresenceMode PresenceMode { get; set; }
        public GameState State { get; set; }
        public VoipTopology VoipTopology { get; set; }
        public GameNetworkTopology NetworkTopology { get; set; }
        public long Settings { get; set; }
        public long ManagerId { get; set; }

        public GameSlotContainer Slots { get; set; }

        public IPEndPoint InternalEndpoint { get; set; }
        public IPEndPoint ExternalEndpoint { get; set; }
        public SortedDictionary<object, object> Attributes { get; set; }
        public long Id { get; set; }
        public string Key { get; set; }
        public ClientType ClientType { get; set; }

        public DateTime LastSeen { get; set; }
        public Dictionary<string, object> Data { get; set; }
    }
}