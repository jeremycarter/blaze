﻿using System;
using System.Collections.Generic;
using System.Net;
using Blaze.Networking.ServerBase;

namespace Blaze.Networking.BF4.Managers
{
    public class ClientSession : ISessionObject
    {
        public ClientSession()
        {
            LastSeen = DateTime.Now;
            Data = new Dictionary<string, object>();
        }

        public string Ticket { get; set; }
        public IPAddress IpAddress { get; set; }
        public ClientType ClientType { get; set; }
        public long Localization { get; set; }
        public string Service { get; set; }

        public long GameId { get; set; }
        public long UserId { get; set; }
        public long UserMode { get; set; }
        public long PersonaId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }

        public string MacAddress { get; set; }
        public IPEndPoint InternalEndpoint { get; set; }
        public IPEndPoint ExternalEndpoint { get; set; }

        public ISecureTcpClient TcpClient { get; set; }
        public long Id { get; set; }
        public string Key { get; set; }
        public DateTime LastSeen { get; set; }
        public Dictionary<string, object> Data { get; set; }
    }
}