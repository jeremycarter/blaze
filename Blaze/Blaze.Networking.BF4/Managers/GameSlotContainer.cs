﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Blaze.Networking.BF4.Managers
{
    public class GameSlotContainer
    {
        private int _slotId;

        public GameSlotContainer()
        {
            Users = new List<UserSlot>();
        }

        public List<UserSlot> Users { get; set; }
        public int Count => Users.Count;

        public UserSlot Add(ClientSession session, SlotType slotType = SlotType.Soldier)
        {
            return Add(session.Id, session.UserId, session.PersonaId, session.Name, slotType);
        }

        public UserSlot Add(long sessionId, long userId, long personaId, string name,
            SlotType slotType = SlotType.Soldier)
        {
            var userSlot = new UserSlot
            {
                Type = slotType,
                Id = Interlocked.Increment(ref _slotId),
                PersonaId = personaId,
                UserId = userId,
                Name = name,
                SessionId = sessionId,
                Joined = DateTime.Now
            };
            Users.Add(userSlot);
            userSlot.Index = Convert.ToInt64(Users.IndexOf(userSlot)) + 1;
            return userSlot;
        }

        public void Remove(ClientSession session)
        {
            Users.RemoveAll(i => i.SessionId == session.Id);
            Users.RemoveAll(i => i.PersonaId == session.PersonaId);
        }

        public void Remove(long userId)
        {
            Users.RemoveAll(i => i.UserId == userId);
        }

        public void RemovePersona(long personaId)
        {
            Users.RemoveAll(i => i.PersonaId == personaId);
        }

        public void RemoveSession(long sessionId)
        {
            Users.RemoveAll(i => i.SessionId == sessionId);
        }
    }

    public class UserSlot
    {
        public int Id { get; set; }
        public long Index { get; set; }
        public DateTime Joined { get; set; }
        public string Name { get; set; }
        public long PersonaId { get; set; }
        public long UserId { get; set; }
        public long SessionId { get; set; }
        public SlotType Type { get; set; }
    }
}