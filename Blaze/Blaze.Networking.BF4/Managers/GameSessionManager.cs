﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using Blaze.Networking.ServerBase;

namespace Blaze.Networking.BF4.Managers
{
    public class GameSessionManager
    {
        private long _sessionId;

        public GameSessionManager()
        {
            Sessions = new HashSet<GameSession>();
        }

        public HashSet<GameSession> Sessions { get; set; }

        public long CreateId()
        {
            var id = Interlocked.Increment(ref _sessionId);
            return Convert.ToInt64(id);
        }

        public GameSession StartTracking(GameSession game)
        {
            var id = CreateId();
            var lookup = Lookup(id);
            if (lookup == null)
            {
                game.Id = id;
                AddOrUpdate(game);
                return game;
            }
            return lookup;
        }

        public GameSession AddOrUpdate(GameSession game)
        {
            var lookup = Lookup(game.Id);
            if (lookup != null)
            {
                lookup.LastSeen = DateTime.Now;
                return lookup;
            }
            game.LastSeen = DateTime.Now;
            lock (Sessions)
            {
                Sessions.Add(game);
            }
            return game;
        }

        public GameSession Lookup(BlazeClientRequest request)
        {
            if (request.Client.SessionId == null) return null;
            return Lookup(request.Client.SessionId.Value);
        }

        public GameSession Lookup(long id)
        {
            return Sessions.Where(i => i.Id == id).OrderByDescending(i => i.LastSeen).FirstOrDefault();
        }

        public GameSession LookupByKey(string key)
        {
            return Sessions.Where(i => i.Key == key).OrderByDescending(i => i.LastSeen).FirstOrDefault();
        }

        public bool Remove(IPEndPoint endpoint)
        {
            lock (Sessions)
            {
                Sessions.RemoveWhere(i => i.ExternalEndpoint.Equals(endpoint));
                Sessions.RemoveWhere(i => i.InternalEndpoint.Equals(endpoint));
            }
            return true;
        }
    }
}