﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using Blaze.Common;
using Blaze.DataBridge;
using Blaze.DataBridge.Local;
using Blaze.Networking;
using Blaze.Networking.BF4.Managers;
using Blaze.Networking.Https;
using Blaze.Networking.Redirector;
using StructureMap.Graph;

namespace Blaze.Server
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Log.Info("Loading configuration");
            ConfigureContainer();

            var hostName = Dns.GetHostName();
            var host = Dns.GetHostEntry(hostName);
            var ipAddress = host.AddressList.OrderByDescending(i=>i.ToString()).FirstOrDefault(i => i.AddressFamily == AddressFamily.InterNetwork);

            Log.Info($"Your current IP address is {ipAddress}");

            // Discard any certificate validation errors
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            // Load certificates required for servers
            var certificate = new X509Certificate2("ea.pfx", "123456");

            var redirectorServer = new RedirectorServer(hostName, ipAddress, certificate);
            redirectorServer.Add(new ServiceRedirection
            {
                ClientType = ClientType.DedicatedServer,
                Product = Product.Battlefield4,
                Port = 10071
            });
            redirectorServer.Add(new ServiceRedirection
            {
                ClientType = ClientType.GameplayUser,
                Product = Product.Battlefield4,
                Port = 10091
            });

            redirectorServer.Start();

            // Setup a https server
            var httpsServer = new HttpsServer(certificate);
            httpsServer.Start();

            // Setup a session manager
            var sessionManager = ObjectFactory.Container.GetInstance<ClientSessionManager>();

            var bf4Server = new BlazeServer(hostName, ipAddress, certificate, 10071, Product.Battlefield4);
            bf4Server.ScanForCommandHandlersIn("Blaze.Networking.BF4.Components.Server");
            bf4Server.ResolveClientSession = request => request.Session = sessionManager.Lookup(request);
            bf4Server.Start();

            var bf4ClientServer = new BlazeServer(hostName, ipAddress, certificate, 10091, Product.Battlefield4);
            bf4ClientServer.ScanForCommandHandlersIn("Blaze.Networking.BF4.Components.Client");
            bf4Server.ResolveClientSession = request => request.Session = sessionManager.Lookup(request);
            bf4ClientServer.Start();

            // Read line for exit
            Log.Info("Type q and enter to exit");

            while (Console.ReadLine() != "q")
            {
            }
            Log.Info("Stopping servers");
            bf4Server.Stop();
        }

        private static void ConfigureContainer()
        {
            // Clear directories
            if (Directory.Exists("Packets")) Directory.Delete("Packets", true);
            if (Directory.Exists("Requests")) Directory.Delete("Requests", true);
            Directory.CreateDirectory("Packets");
            Directory.CreateDirectory("Requests");

            // Load all assemblies into the app domain that haven't been loaded yet
            LoadAllReferencedAssemblies();

            ObjectFactory.Build(c =>
            {
                c.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });

                c.For<IDataBridge>().Use<LocalDataBridge>();
                c.For<Cache>().Use<Cache>().Singleton();
                c.For<ClientSessionManager>().Use<ClientSessionManager>().Singleton();
                c.For<GameSessionManager>().Use<GameSessionManager>().Singleton();
            });
        }

        private static List<Assembly> LoadAllReferencedAssemblies()
        {
            var domain = AppDomain.CurrentDomain;
            var files = Directory.GetFiles(domain.RelativeSearchPath ?? domain.BaseDirectory, "*.dll",
                SearchOption.TopDirectoryOnly);
            return files.Select(file => AppDomain.CurrentDomain.Load(AssemblyName.GetAssemblyName(file))).ToList();
        }
    }
}