﻿namespace Blaze.Protocol
{
    public class BlazePacket
    {
        public ushort Length { get; set; }
        public ushort Component { get; set; }
        public ushort Command { get; set; }
        public ushort Error { get; set; }
        public ushort MessageType { get; set; }
        public ushort Id { get; set; }
        public ushort ExtLength { get; set; }
        public byte[] Data { get; set; }
        public byte[] Header { get; set; }
        public TdfContainer Content { get; set; }

        public byte[] ToBytes()
        {
            var writer = new BlazePacketWriter();
            return writer.ToBytes(this);
        }
    }
}