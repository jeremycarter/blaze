﻿namespace Blaze.Protocol
{
    public enum TdfType
    {
        Integer = 0,
        String = 1,
        Binary = 2,
        Struct = 3,
        List = 4,
        Map = 5,
        Union = 6,
        IntegerList = 7,
        DoubleValue = 8,
        TrippleValue = 9,
        Float = 0xA,
        TimeValue = 0xB,
        Max = 0xC,
        Hexadecimal = 9999
    }
}