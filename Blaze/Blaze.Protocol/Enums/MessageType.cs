﻿namespace Blaze.Protocol
{
    public enum MessageType : ushort
    {
        Message = 0x0000,
        Reply = 0x1000,
        Notification = 0x2000,
        ErrorReply = 0x3000
    }
}