﻿using System;
using System.Collections.Generic;
using Blaze.Protocol.Types;

namespace Blaze.Protocol
{
    public static class BlazeExtensions
    {
        public static List<byte> ToBytes(this float value)
        {
            var bytes = new List<byte>();
            var buff = BitConverter.GetBytes(value);
            for (var i = 0; i < 4; i++)
            {
                bytes.Add(buff[3 - i]);
            }
            return bytes;
        }

        public static List<byte> ToBytes(this int value)
        {
            return ((long) value).ToBytes();
        }

        public static List<byte> ToBytes(this long value)
        {
            var bytes = new List<byte>();
            if (value < 0x40)
            {
                bytes.Add((byte) (value & 0xFF));
            }
            else
            {
                var curbyte = (byte) ((value & 0x3F) | 0x80);
                bytes.Add(curbyte);
                var currshift = value >> 6;
                while (currshift >= 0x80)
                {
                    curbyte = (byte) ((currshift & 0x7F) | 0x80);
                    currshift >>= 7;
                    bytes.Add(curbyte);
                }
                bytes.Add((byte) currshift);
            }
            return bytes;
        }

        public static TdfType ToType(this uint tag)
        {
            return (TdfType) (tag & 0xFF);
        }

        public static byte[] ToTag(this string label)
        {
            var res = new byte[3];
            while (label.Length < 4)
                label += '\0';
            if (label.Length > 4)
                label = label.Substring(0, 4);
            var buff = new byte[4];
            for (var i = 0; i < 4; i++)
                buff[i] = (byte) label[i];
            res[0] |= (byte) ((buff[0] & 0x40) << 1);
            res[0] |= (byte) ((buff[0] & 0x10) << 2);
            res[0] |= (byte) ((buff[0] & 0x0F) << 2);
            res[0] |= (byte) ((buff[1] & 0x40) >> 5);
            res[0] |= (byte) ((buff[1] & 0x10) >> 4);

            res[1] |= (byte) ((buff[1] & 0x0F) << 4);
            res[1] |= (byte) ((buff[2] & 0x40) >> 3);
            res[1] |= (byte) ((buff[2] & 0x10) >> 2);
            res[1] |= (byte) ((buff[2] & 0x0C) >> 2);

            res[2] |= (byte) ((buff[2] & 0x03) << 6);
            res[2] |= (byte) ((buff[3] & 0x40) >> 1);
            res[2] |= (byte) (buff[3] & 0x1F);
            return res;
        }

        public static string ToLabel(this uint tag)
        {
            tag = tag & 0xFFFFFF00;
            var buff = new List<byte>(BitConverter.GetBytes(tag));
            buff.Reverse();
            var res = new byte[4];
            res[0] |= (byte) ((buff[0] & 0x80) >> 1);
            res[0] |= (byte) ((buff[0] & 0x40) >> 2);
            res[0] |= (byte) ((buff[0] & 0x30) >> 2);
            res[0] |= (byte) ((buff[0] & 0x0C) >> 2);

            res[1] |= (byte) ((buff[0] & 0x02) << 5);
            res[1] |= (byte) ((buff[0] & 0x01) << 4);
            res[1] |= (byte) ((buff[1] & 0xF0) >> 4);

            res[2] |= (byte) ((buff[1] & 0x08) << 3);
            res[2] |= (byte) ((buff[1] & 0x04) << 2);
            res[2] |= (byte) ((buff[1] & 0x03) << 2);
            res[2] |= (byte) ((buff[2] & 0xC0) >> 6);

            res[3] |= (byte) ((buff[2] & 0x20) << 1);
            res[3] |= (byte) (buff[2] & 0x1F);

            var s = string.Empty;
            for (var i = 0; i < 4; i++)
            {
                if (res[i] == 0)
                    res[i] = 0x20;
                s += (char) res[i];
            }
            return s.Trim();
        }

        public static TdfContainer ToContainer(this ITdf item)
        {
            var container = new TdfContainer();
            container.Add(item);
            return container;
        }

        public static TdfContainer ToContainer(this IEnumerable<ITdf> items)
        {
            var container = new TdfContainer();
            container.AddRange(items);
            return container;
        }
    }
}