﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Blaze.Protocol
{
    public static class StreamExtensions
    {
        public static byte PeekByte(this Stream stream)
        {
            if (stream.CanRead && stream.CanSeek)
            {
                var originalPosition = stream.Position;
                if (stream.Position != stream.Length)
                {
                    var peek = (byte) stream.ReadByte();
                    stream.Seek(originalPosition, SeekOrigin.Begin);
                    return peek;
                }
            }
            return default(byte);
        }

        public static byte[] ReadBytes(this Stream stream, int count)
        {
            var bytes = new List<byte>();

            try
            {
                if (stream.CanRead && stream.CanSeek)
                {
                    for (var i = 0; i < count; i++)
                    {
                        if (stream.Position != stream.Length)
                        {
                            bytes.Add((byte) stream.ReadByte());
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            return bytes.ToArray();
        }

        public static byte[] PeekBytes(this Stream stream, int count)
        {
            var bytes = new List<byte>();

            try
            {
                if (stream.CanRead && stream.CanSeek)
                {
                    var originalPosition = stream.Position;
                    for (var i = 0; i < count; i++)
                    {
                        if (stream.Position != stream.Length)
                        {
                            bytes.Add((byte) stream.ReadByte());
                        }
                    }
                    stream.Seek(originalPosition, SeekOrigin.Begin);
                }
            }
            catch (Exception)
            {
            }
            return bytes.ToArray();
        }
    }
}