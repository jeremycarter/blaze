﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blaze.Protocol
{
    public static class ByteExtensions
    {
        public static string ToHex(this IEnumerable<byte> arr)
        {
            if (arr == null) return string.Empty;
            var ba = arr.ToArray();
            var hex = new StringBuilder(ba.Length*2);
            foreach (var b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public static byte[] ToByteArray(this string hex)
        {
            var numberChars = hex.Length;
            var bytes = new byte[numberChars/2];
            for (var i = 0; i < numberChars; i += 2)
                bytes[i/2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}