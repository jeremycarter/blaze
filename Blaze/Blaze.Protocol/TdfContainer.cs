﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blaze.Common;
using Blaze.Protocol.Types;
using Blaze.Protocol.Writers;

namespace Blaze.Protocol
{
    public class TdfContainer : List<ITdf>
    {
        public TdfContainer()
        {
        }

        public TdfContainer(IEnumerable<ITdf> items)
        {
            if (items != null)
            {
                AddRange(items);
            }
        }

        public bool ContainsKey(string key)
        {
            try
            {
                return this.Any(i => i.Label == key);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool GetBoolean(string key)
        {
            try
            {
                var tdf = (TdfInteger) this.FirstOrDefault(i => i.Label == key);
                return tdf.Value == 1;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public long GetInteger(string key)
        {
            var tdf = (TdfInteger) this.FirstOrDefault(i => i.Label == key);
            return tdf.Value;
        }

        public string GetString(string key)
        {
            var tdf = (TdfString) this.FirstOrDefault(i => i.Label == key);
            return tdf.Value;
        }

        public ITdf GetItem(string key)
        {
            return this.FirstOrDefault(i => i.Label == key);
        }

        public T GetItem<T>(string key)
        {
            try
            {
                return (T) this.FirstOrDefault(i => i.Label == key);
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public byte[] ToBytes()
        {
            var contentBytes = new List<byte>();
            if (this.Any())
            {
                foreach (var tdf in this)
                {
                    if (tdf == null) continue;

                    var tdfBytes = new List<byte>();

                    var currentLen = contentBytes.Count;

                    tdf.Position = currentLen;
                    var writer = TdfWriterFactory.Find(tdf.Type);
                    if (writer != null)
                    {
                        try
                        {
                            tdfBytes.AddRange(writer.Write(tdf, true));

                            if (tdfBytes.Any())
                            {
                                //Console.WriteLine($"{tdf.Label} at {tdf.Position} length {tdfBytes.Count}");
                                //Console.WriteLine($"{tdfBytes.ToArray().ToHex()}");

                                contentBytes.AddRange(tdfBytes);
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex);
                            Console.WriteLine($"{ex.Message}:@:{contentBytes.Count}");
                        }
                    }
                }
            }

            return contentBytes.ToArray();
        }
    }
}