﻿using Blaze.Protocol.Types;

namespace Blaze.Protocol.Readers
{
    public class TdfDoubleValueReader : ITdfReader
    {
        public ITdf Read(uint tag, BlazeStream stream)
        {
            var tdf = new TdfDoubleValue();

            tdf.Value1 = stream.ReadInt();
            tdf.Value2 = stream.ReadInt();

            return tdf;
        }
    }
}