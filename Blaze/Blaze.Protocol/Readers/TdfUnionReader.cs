﻿using Blaze.Protocol.Types;

namespace Blaze.Protocol.Readers
{
    public class TdfUnionReader : ITdfReader
    {
        public ITdf Read(uint tag, BlazeStream stream)
        {
            var tdf = new TdfUnion();
            var type = (byte) stream.ReadByte();
            tdf.SubType = type;

            if (type != 0x7F)
            {
                tdf.Content = TdfReader.Read(stream);
            }

            return tdf;
        }
    }
}