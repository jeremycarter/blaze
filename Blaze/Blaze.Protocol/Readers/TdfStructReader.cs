﻿using System;
using System.Collections.Generic;
using System.IO;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Readers
{
    public class TdfStructReader : ITdfReader
    {
        public ITdf Read(uint tag, BlazeStream stream)
        {
            var tdf = new TdfStruct();
            tdf.Values = new List<ITdf>();
            tdf.Label = "VALU";
            var count = 0;
            byte b = 0;
            var has2 = false;

            while ((b = (byte) stream.ReadByte()) != 0)
            {
                try
                {
                    if (b != 2)
                        stream.Seek(-1, SeekOrigin.Current);
                    else
                        has2 = true;

                    tdf.Values.Add(TdfReader.Read(stream));
                    count += 1;
                }
                catch (Exception)
                {
                    break;
                }
            }

            tdf.StartsWith2 = has2;
            return tdf;
        }
    }
}