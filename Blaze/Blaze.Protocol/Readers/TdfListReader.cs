﻿using System.Collections.Generic;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Readers
{
    public class TdfListReader : ITdfReader
    {
        public ITdf Read(uint tag, BlazeStream stream)
        {
            var subType = (TdfType) stream.ReadByte();

            switch (subType)
            {
                case TdfType.String:
                    var tdfListString = new TdfList<string>
                    {
                        Label = "VALU",
                        Values = new List<string>(),
                        SubType = subType,
                        Count = (int) stream.ReadInt()
                    };
                    IterateString(tdfListString, stream);
                    return tdfListString;
                case TdfType.Integer:
                    var tdfIntegerList = new TdfList<long>
                    {
                        Label = "VALU",
                        Values = new List<long>(),
                        SubType = subType,
                        Count = (int) stream.ReadInt()
                    };
                    IterateInteger(tdfIntegerList, stream);
                    return tdfIntegerList;

                case TdfType.Struct:
                    var tdfStructs = new TdfList<TdfStruct>
                    {
                        Label = "VALU",
                        Values = new List<TdfStruct>(),
                        SubType = subType,
                        Count = (int) stream.ReadInt()
                    };
                    IterateStruct(tdfStructs, stream);
                    return tdfStructs;

                default:
                    var tdf = new TdfList<ITdf>
                    {
                        Label = "VALU",
                        Values = new List<ITdf>(),
                        SubType = subType,
                        Count = (int) stream.ReadInt()
                    };
                    IterateTdfs(subType, tdf, stream);
                    return tdf;
            }
        }

        private void IterateStruct(TdfList<TdfStruct> tdf, BlazeStream stream)
        {
            for (var i = 0; i < tdf.Count; i++)
            {
                var reader = new TdfStructReader();
                var struc = (TdfStruct) reader.Read(tdf.Tag, stream);
                tdf.Values.Add(struc);
            }
        }

        private void IterateTdfs(TdfType type, TdfList<ITdf> tdf, BlazeStream stream)
        {
            for (var i = 0; i < tdf.Count; i++)
            {
                var reader = TdfReaderFactory.Find(type);
                tdf.Values.Add(reader.Read(tdf.Tag, stream));
            }
        }

        private void IterateInteger(TdfList<long> tdf, BlazeStream stream)
        {
            for (var i = 0; i < tdf.Count; i++)
            {
                tdf.Values.Add(stream.ReadInt());
            }
        }

        private void IterateString(TdfList<string> tdf, BlazeStream stream)
        {
            for (var i = 0; i < tdf.Count; i++)
            {
                tdf.Values.Add(stream.ReadString());
            }
        }
    }
}