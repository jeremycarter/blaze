﻿using System;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Readers
{
    public static class TdfReader
    {
        public static ITdf Read(BlazeStream stream)
        {
            var tag = stream.ReadUInt();
            var type = tag.ToType();

            ITdf item = null;
            try
            {
                var reader = TdfReaderFactory.Find(type);
                if (reader != null)
                {
                    item = reader.Read(tag, stream);
                    item.Label = tag.ToLabel();
                    item.Tag = tag;
                    item.Type = type;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return item;
        }
    }
}