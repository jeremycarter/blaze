﻿using Blaze.Protocol.Types;

namespace Blaze.Protocol.Readers
{
    public class TdfIntegerReader : ITdfReader
    {
        public ITdf Read(uint tag, BlazeStream stream)
        {
            var tdf = new TdfInteger();
            tdf.Value = stream.ReadInt();
            return tdf;
        }
    }
}