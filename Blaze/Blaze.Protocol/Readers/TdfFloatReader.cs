﻿using System;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Readers
{
    public class TdfFloatReader : ITdfReader
    {
        public ITdf Read(uint tag, BlazeStream stream)
        {
            var tdf = new TdfFloat();

            var buff = stream.ReadBytes(4);
            var buffr = new byte[4];
            for (var i = 0; i < 4; i++)
                buffr[i] = buff[3 - i];
            tdf.Value = BitConverter.ToSingle(buffr, 0);
            return tdf;
        }
    }
}