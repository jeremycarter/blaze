﻿using System.Collections.Generic;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Readers
{
    public class TdfIntegerListReader : ITdfReader
    {
        public ITdf Read(uint tag, BlazeStream stream)
        {
            var tdf = new TdfIntegerList
            {
                Values = new List<long>(),
                Count = stream.ReadInt()
            };

            for (var i = 0; i < tdf.Count; i++)
            {
                tdf.Values.Add(stream.ReadInt());
            }

            return tdf;
        }
    }
}