﻿using Blaze.Protocol.Types;

namespace Blaze.Protocol.Readers
{
    public class TdfStringReader : ITdfReader
    {
        public ITdf Read(uint tag, BlazeStream stream)
        {
            var tdf = new TdfString();
            tdf.Value = stream.ReadString();
            return tdf;
        }
    }
}