﻿using Blaze.Protocol.Types;

namespace Blaze.Protocol.Readers
{
    public class TdfTrippleValueReader : ITdfReader
    {
        public ITdf Read(uint tag, BlazeStream stream)
        {
            var tdf = new TdfTrippleValue();

            tdf.Value1 = stream.ReadInt();
            tdf.Value2 = stream.ReadInt();
            tdf.Value3 = stream.ReadInt();

            return tdf;
        }
    }
}