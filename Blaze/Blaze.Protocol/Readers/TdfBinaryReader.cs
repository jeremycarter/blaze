﻿using System.Linq;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Readers
{
    public class TdfBinaryReader : ITdfReader
    {
        public ITdf Read(uint tag, BlazeStream stream)
        {
            var tdf = new TdfBinary();
            var length = (int) stream.ReadInt();
            tdf.Value = stream.ReadBytes(length).ToList();
            return tdf;
        }
    }
}