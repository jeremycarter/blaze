﻿using Blaze.Protocol.Types;

namespace Blaze.Protocol.Readers
{
    public interface ITdfReader
    {
        ITdf Read(uint tag, BlazeStream stream);
    }
}