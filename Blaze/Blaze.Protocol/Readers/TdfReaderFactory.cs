﻿namespace Blaze.Protocol.Readers
{
    public static class TdfReaderFactory
    {
        public static ITdfReader Find(TdfType type)
        {
            ITdfReader reader = null;
            switch (type)
            {
                case TdfType.Integer:
                    reader = new TdfIntegerReader();
                    break;
                case TdfType.String:
                    reader = new TdfStringReader();
                    break;
                case TdfType.Binary:
                    reader = new TdfBinaryReader();
                    break;
                case TdfType.Struct:
                    reader = new TdfStructReader();
                    break;
                case TdfType.List:
                    reader = new TdfListReader();
                    break;
                case TdfType.Map:
                    reader = new TdfMapReader();
                    break;
                case TdfType.Union:
                    reader = new TdfUnionReader();
                    break;
                case TdfType.IntegerList:
                    reader = new TdfIntegerListReader();
                    break;
                case TdfType.DoubleValue:
                    reader = new TdfDoubleValueReader();
                    break;
                case TdfType.TrippleValue:
                    reader = new TdfTrippleValueReader();
                    break;
                case TdfType.Float:
                    reader = new TdfFloatReader();
                    break;
                default:
                    reader = null;
                    break;
            }
            return reader;
        }
    }
}