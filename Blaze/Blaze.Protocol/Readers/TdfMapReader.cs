﻿using System;
using System.Collections.Generic;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Readers
{
    public class TdfMapReader : ITdfReader
    {
        public ITdf Read(uint tag, BlazeStream stream)
        {
            var tdf = new TdfMap();
            tdf.Map = new SortedDictionary<object, object>();
            tdf.KeyType = (TdfType) stream.ReadByte();
            tdf.ValueType = (TdfType) stream.ReadByte();
            tdf.Count = (int) stream.ReadInt();

            Iterate(tdf, stream);

            return tdf;
        }

        private void Iterate(TdfMap tdf, BlazeStream stream)
        {
            Func<TdfType, object> readListItem = type =>
            {
                object item = null;
                switch (type)
                {
                    case TdfType.Integer:
                        item = stream.ReadInt();
                        break;

                    case TdfType.String:
                        item = stream.ReadString();
                        break;

                    case TdfType.Struct:
                        var reader = new TdfStructReader();
                        item = reader.Read(tdf.Tag, stream);
                        break;
                }

                return item;
            };

            for (var i = 0; i < tdf.Count; i++)
            {
                var key = readListItem(tdf.KeyType);
                var value = readListItem(tdf.ValueType);
                if (key != null && value != null)
                {
                    tdf.Map.Add(key, value);
                }
            }
        }
    }
}