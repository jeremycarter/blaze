﻿using System.Collections.Generic;
using System.Linq;

namespace Blaze.Protocol.Types
{
    public class TdfBinary : ITdf
    {
        public TdfBinary()
        {
            Type = TdfType.Binary;
        }

        public TdfBinary(string label, IEnumerable<byte> value)
        {
            Type = TdfType.Binary;
            Label = label;
            if (value == null) value = new List<byte>();
            Value = value.ToList();
            Count = value.Count();
        }

        public long Count { get; set; }
        public List<byte> Value { get; set; }
        public string Label { get; set; }
        public TdfType Type { get; set; }
        public uint Tag { get; set; }
        public long Position { get; set; }
    }
}