﻿namespace Blaze.Protocol.Types
{
    public interface ITdf
    {
        string Label { get; set; }
        TdfType Type { get; set; }
        uint Tag { get; set; }
        long Position { get; set; }
    }
}