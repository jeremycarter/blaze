﻿namespace Blaze.Protocol.Types
{
    public class TdfInteger : ITdf
    {
        public TdfInteger()
        {
            Type = TdfType.Integer;
        }

        public TdfInteger(string label, bool value)
        {
            Label = label;
            Value = value ? 1 : 0;
            Type = TdfType.Integer;
        }

        public TdfInteger(string label, int value)
        {
            Label = label;
            Value = value;
            Type = TdfType.Integer;
        }

        public TdfInteger(string label, ulong value)
        {
            Label = label;
            Value = (long) value;
            Type = TdfType.Integer;
        }

        public TdfInteger(string label, long value)
        {
            Label = label;
            Value = value;
            Type = TdfType.Integer;
        }

        public long Value { get; set; }
        public string Label { get; set; }
        public TdfType Type { get; set; }
        public uint Tag { get; set; }
        public long Position { get; set; }
    }
}