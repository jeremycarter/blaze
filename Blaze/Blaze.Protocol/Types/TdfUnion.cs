﻿namespace Blaze.Protocol.Types
{
    public class TdfUnion : ITdf
    {
        public TdfUnion()
        {
            Type = TdfType.Union;
        }

        public TdfUnion(string label, uint subType, ITdf content)
        {
            Label = label;
            Content = content;
            SubType = subType;
            Type = TdfType.Union;
        }

        public uint SubType { get; set; }
        public ITdf Content { get; set; }
        public string Label { get; set; }
        public TdfType Type { get; set; }
        public uint Tag { get; set; }
        public long Position { get; set; }
    }
}