﻿using System;
using System.Collections.Generic;

namespace Blaze.Protocol.Types
{
    public class TdfStruct : ITdf
    {
        public TdfStruct()
        {
            Type = TdfType.Struct;
        }

        public TdfStruct(string label)
        {
            Label = label;
            Values = new List<ITdf>();
            Type = TdfType.Struct;
        }

        public TdfStruct(string label, List<ITdf> values = null)
        {
            Label = label;
            if (values == null)
            {
                Values = new List<ITdf>();
            }
            else
            {
                Values = values;
            }
            Type = TdfType.Struct;
        }

        public TdfStruct(string label, List<ITdf> values = null, bool startsWith2 = false)
        {
            Label = label;
            if (values == null)
            {
                Values = new List<ITdf>();
            }
            else
            {
                Values = values;
            }
            Type = TdfType.Struct;
            StartsWith2 = startsWith2;
        }

        public List<ITdf> Values { get; set; }
        public bool StartsWith2 { get; set; }

        public string Label { get; set; }
        public TdfType Type { get; set; }
        public uint Tag { get; set; }
        public long Position { get; set; }

        public T Find<T>(Predicate<ITdf> predicate)
        {
            return (T) Values.Find(predicate);
        }
    }
}