﻿using System.Collections.Generic;

namespace Blaze.Protocol.Types
{
    public class TdfMap : ITdf
    {
        public TdfMap()
        {
            Type = TdfType.Map;
        }

        public TdfMap(string label, SortedDictionary<object, object> values)
        {
            Type = TdfType.Map;
            Label = label;
            KeyType = TdfType.String;
            ValueType = TdfType.String;
            Map = values;
        }

        public TdfMap(string label, TdfType keyType, TdfType valueType, SortedDictionary<object, object> values)
        {
            Type = TdfType.Map;
            Label = label;
            KeyType = keyType;
            ValueType = valueType;
            Map = values;
        }

        public TdfType KeyType { get; set; }
        public TdfType ValueType { get; set; }
        public int Count { get; set; }
        public SortedDictionary<object, object> Map { get; set; }
        public string Label { get; set; }
        public TdfType Type { get; set; }
        public uint Tag { get; set; }
        public long Position { get; set; }

        public bool ContainsKey(string key)
        {
            return Map.ContainsKey(key);
        }

        public T GetValue<T>(string key)
        {
            return (T) Map[key];
        }

        public object GetValue(string key)
        {
            return Map[key];
        }

        public void Recount()
        {
            Count = Map.Count;
        }
    }
}