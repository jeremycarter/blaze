﻿using System.Collections.Generic;

namespace Blaze.Protocol.Types
{
    public class TdfIntegerList : ITdf
    {
        public TdfIntegerList()
        {
            Type = TdfType.IntegerList;
        }

        public TdfIntegerList(string label, List<long> values)
        {
            if (values == null)
            {
                values = new List<long>();
            }
            Type = TdfType.IntegerList;
            Label = label;
            Values = values;
            Count = values.Count;
        }

        public long Count { get; set; }
        public List<long> Values { get; set; }
        public string Label { get; set; }
        public TdfType Type { get; set; }
        public uint Tag { get; set; }
        public long Position { get; set; }
    }
}