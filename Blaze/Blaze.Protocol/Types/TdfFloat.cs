﻿namespace Blaze.Protocol.Types
{
    public class TdfFloat : ITdf
    {
        public TdfFloat()
        {
            Type = TdfType.Float;
        }

        public TdfFloat(string label, float value)
        {
            Label = label;
            Value = value;
            Type = TdfType.Float;
        }

        public float Value { get; set; }
        public string Label { get; set; }
        public TdfType Type { get; set; }
        public uint Tag { get; set; }
        public long Position { get; set; }
    }
}