﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Blaze.Protocol.Types
{
    public class TdfList<T> : ITdfList
    {
        public TdfList()
        {
            Type = TdfType.List;
        }

        public TdfList(string label, TdfType subType, bool stub = false)
        {
            Type = TdfType.List;
            Label = label;
            Values = new List<T>();
            SubType = subType;
            Stub = stub;
        }

        public TdfList(string label, TdfType subType, IEnumerable<T> values, bool stub = false)
        {
            Type = TdfType.List;
            Label = label;
            Values = values.ToList();
            SubType = subType;
            Count = values.Count();
            Stub = stub;
        }

        public List<T> Values { get; set; }

        public string Label { get; set; }
        public TdfType Type { get; set; }
        public TdfType SubType { get; set; }
        public uint Tag { get; set; }
        public int Count { get; set; }
        public bool Stub { get; set; }
        public long Position { get; set; }

        public void Recount()
        {
            if (Values == null) Count = 0;
            Count = Values.Count;
        }

        public T FirstOrDefault()
        {
            return Values.FirstOrDefault();
        }

        public T FirstOrDefault(Func<T, bool> predicate)
        {
            return Values.FirstOrDefault(predicate);
        }

        public ITdf GetValue(string key)
        {
            try
            {
                var tdfs = Values.OfType<ITdf>();
                return tdfs.FirstOrDefault(i => i.Label == key);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    public interface ITdfList : ITdf
    {
        int Count { get; set; }
        TdfType SubType { get; set; }
        bool Stub { get; set; }
        void Recount();
    }
}