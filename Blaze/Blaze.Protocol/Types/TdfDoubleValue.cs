﻿namespace Blaze.Protocol.Types
{
    public class TdfDoubleValue : ITdf
    {
        public TdfDoubleValue()
        {
            Type = TdfType.DoubleValue;
        }

        public TdfDoubleValue(string label, long value1, long value2)
        {
            Type = TdfType.DoubleValue;
            Label = label;
            Value1 = value1;
            Value2 = value2;
        }

        public long Value1 { get; set; }
        public long Value2 { get; set; }
        public string Label { get; set; }
        public TdfType Type { get; set; }
        public uint Tag { get; set; }
        public long Position { get; set; }
    }
}