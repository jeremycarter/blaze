﻿namespace Blaze.Protocol.Types
{
    public class TdfTrippleValue : ITdf
    {
        public TdfTrippleValue()
        {
            Type = TdfType.TrippleValue;
        }

        public TdfTrippleValue(string label, long value1, long value2, long value3)
        {
            Type = TdfType.TrippleValue;
            Label = label;
            Value1 = value1;
            Value2 = value2;
            Value3 = value3;
        }

        public long Value1 { get; set; }
        public long Value2 { get; set; }
        public long Value3 { get; set; }
        public string Label { get; set; }
        public TdfType Type { get; set; }
        public uint Tag { get; set; }
        public long Position { get; set; }
    }
}