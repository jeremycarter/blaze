﻿namespace Blaze.Protocol.Types
{
    public class TdfHex : ITdf
    {
        public TdfHex(string hex)
        {
            Type = TdfType.Hexadecimal;
            Value = hex;
        }

        public string Value { get; set; }

        public string Label { get; set; }
        public TdfType Type { get; set; }
        public uint Tag { get; set; }
        public long Position { get; set; }
    }
}