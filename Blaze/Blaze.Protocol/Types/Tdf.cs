﻿namespace Blaze.Protocol.Types
{
    public class TdfString : ITdf
    {
        public TdfString()
        {
            Type = TdfType.String;
        }

        public TdfString(string label, string value)
        {
            Label = label;
            Value = value;
            Type = TdfType.String;
        }

        public string Value { get; set; }
        public string Label { get; set; }
        public TdfType Type { get; set; }
        public uint Tag { get; set; }
        public long Position { get; set; }
    }
}