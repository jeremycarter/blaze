﻿using System.Collections.Generic;
using System.Linq;

namespace Blaze.Protocol
{
    public class BlazePacketWriter
    {
        public byte[] ToBytes(BlazePacket packet)
        {
            var bytes = new List<byte>();

            var contentBytes = packet.Content?.ToBytes() ?? new byte[0];

            bytes.Add((byte) ((contentBytes.Length & 0xFFFF) >> 8));
            bytes.Add((byte) (contentBytes.Length & 0xFF));
            bytes.Add((byte) (packet.Component >> 8)); // 2
            bytes.Add((byte) (packet.Component & 0xFF));
            bytes.Add((byte) (packet.Command >> 8)); // 4
            bytes.Add((byte) (packet.Command & 0xFF));
            bytes.Add((byte) (packet.Error >> 8)); // 6
            bytes.Add((byte) (packet.Error & 0xFF));
            bytes.Add((byte) (packet.MessageType >> 8)); // 8
            bytes.Add((byte) (packet.MessageType & 0xFF));
            bytes.Add((byte) (packet.Id >> 8)); // 10
            bytes.Add((byte) (packet.Id & 0xFF));

            if ((packet.MessageType & 0x10) != 0)
            {
                bytes.Add((byte) ((contentBytes.Length & 0xFF000000) >> 24));
                bytes.Add((byte) ((contentBytes.Length & 0x00FF0000) >> 16));
            }

            if (contentBytes != null && contentBytes.Any())
            {
                bytes.AddRange(contentBytes);
            }

            return bytes.ToArray();
        }
    }
}