﻿using System;
using System.Linq;
using Blaze.Common;
using Blaze.Protocol.Readers;

namespace Blaze.Protocol
{
    public class BlazeStreamParser
    {
        private byte[] _array;
        private BlazeStream _content;
        private BlazeStream _stream;

        public BlazePacket Parse(byte[] array)
        {
            if (array == null) throw new ArrayTypeMismatchException("Blaze stream array is null.");
            if (array.Length == 0) throw new ArrayTypeMismatchException("Blaze stream array is empty.");

            _array = array;
            _stream = new BlazeStream(array);

            var packet = new BlazePacket();
            packet.Length = _stream.ReadUShort();
            packet.Component = _stream.ReadUShort();
            packet.Command = _stream.ReadUShort();
            packet.Error = _stream.ReadUShort();
            packet.MessageType = _stream.ReadUShort();

            packet.Id = _stream.ReadUShort();

            if ((packet.MessageType & 0x10) != 0)
            {
                packet.ExtLength = _stream.ReadUShort();
            }
            else
            {
                packet.ExtLength = 0;
            }

            var len = packet.Length + (packet.ExtLength << 16);

            if (_stream.Length < packet.Length) return packet;

            packet.Header = _array.Take((int) _stream.Position).ToArray();

            if (packet.Length > 0)
            {
                // Skip over any zero bytes that may be present until the first non zero byte
                if (_stream.PeekByte() == 0)
                {
                    do
                    {
                        if (_stream.PeekByte() == 0)
                        {
                            _stream.ReadByte();
                        }
                    } while (_stream.PeekByte() != 0);
                }

                packet.Data = _stream.ReadBytes(packet.Length);
                packet.Content = ParseContent(packet.Data);
            }
            return packet;
        }

        public TdfContainer ParseContent(byte[] data)
        {
            if (data == null) return null;
            if (data.Length == 0) return null;

            var items = new TdfContainer();

            _content = new BlazeStream(data);

            try
            {
                while (_content.Position < _content.Length - 4)
                {
                    var startPosition = _content.Position;
                    try
                    {
                        var tdf = TdfReader.Read(_content);
                        if (tdf != null)
                        {
                            tdf.Position = startPosition;
                            items.Add(tdf);
                        }
                        else
                        {
                            // A null tdf. Everything after this will probably be useless
                            var last = items.LastOrDefault();
                            if (last != null)
                            {
                                Log.Info($"Tdf failed to parse at {startPosition}, last {last.Label} @ {last.Position}");
                            }
                            break;
                        }
                        //var length = _content.Position - startPosition;
                        //var bytes = data.Skip((int)startPosition).Take((int)length);
                        //Console.WriteLine($"{tdf.Label} at {tdf.Position} length {bytes.Count()}");
                        //Console.WriteLine($"{bytes.ToArray().ToHex()}");
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                        // Break the loop
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}:@:{_content.Position.ToString("X")}");
            }

            return items;
        }
    }
}