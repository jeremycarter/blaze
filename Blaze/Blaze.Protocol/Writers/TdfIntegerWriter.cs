﻿using System.Collections.Generic;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Writers
{
    public class TdfIntegerWriter : ITdfWriter
    {
        public TdfType ParentType { get; set; }
        public string ParentName { get; set; }

        public List<byte> Write(ITdf input, bool includeHeader)
        {
            var bytes = new List<byte>();

            var tdf = (TdfInteger) input;

            if (includeHeader)
            {
                var tagBytes = tdf.Label.ToTag();
                bytes.AddRange(tagBytes);
                bytes.Add((byte) tdf.Type);
            }

            bytes.AddRange(tdf.Value.ToBytes());

            return bytes;
        }
    }
}