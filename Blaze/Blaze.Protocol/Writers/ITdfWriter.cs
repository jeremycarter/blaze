﻿using System.Collections.Generic;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Writers
{
    public interface ITdfWriter
    {
        TdfType ParentType { get; set; }
        string ParentName { get; set; }
        List<byte> Write(ITdf tdf, bool includeHeader);
    }
}