﻿using System.Collections.Generic;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Writers
{
    public class TdfListWriter : ITdfWriter
    {
        public TdfType ParentType { get; set; }
        public string ParentName { get; set; }

        public List<byte> Write(ITdf input, bool includeHeader)
        {
            var tdf = (ITdfList) input;
            tdf.Recount();
            var bytes = new List<byte>();

            if (includeHeader)
            {
                var tagBytes = tdf.Label.ToTag();
                bytes.AddRange(tagBytes);
                bytes.Add((byte) tdf.Type);
                bytes.Add((byte) tdf.SubType);
                bytes.AddRange(tdf.Count.ToBytes());
            }
            else
            {
                bytes.Add((byte) tdf.SubType);
                bytes.AddRange(tdf.Count.ToBytes());
            }

            if (input is TdfList<ITdf>)
            {
                WriteTdfs(bytes, (TdfList<ITdf>) tdf, tdf);
            }
            else if (input is TdfList<TdfStruct>)
            {
                WriteStructs(bytes, (TdfList<TdfStruct>) tdf, tdf);
            }
            else
            {
                switch (tdf.SubType)
                {
                    case TdfType.Integer:
                        WriteIntegerBytes(bytes, (TdfList<long>) tdf, tdf);
                        break;
                    case TdfType.String:
                        WriteStringBytes(bytes, (TdfList<string>) tdf, tdf);
                        break;
                    case TdfType.Struct:
                        WriteStructs(bytes, (TdfList<TdfStruct>) tdf, tdf);
                        break;
                }
            }

            return bytes;
        }

        public void WriteIntegerBytes(List<byte> bytes, TdfList<long> tdf, ITdf parent)
        {
            var writer = TdfWriterFactory.Find(tdf.SubType);
            writer.ParentType = parent.Type;
            writer.ParentName = parent.Label;
            for (var i = 0; i < tdf.Count; i++)
            {
                var rawItem = tdf.Values[i];
                var fakeLong = new TdfInteger {Value = rawItem};
                bytes.AddRange(writer.Write(fakeLong, false));
            }
        }

        public void WriteStringBytes(List<byte> bytes, TdfList<string> tdf, ITdf parent)
        {
            var writer = TdfWriterFactory.Find(tdf.SubType);
            writer.ParentType = parent.Type;
            writer.ParentName = parent.Label;
            for (var i = 0; i < tdf.Count; i++)
            {
                var rawItem = tdf.Values[i];
                var fakeLong = new TdfString {Value = rawItem};
                bytes.AddRange(writer.Write(fakeLong, false));
            }
        }

        public void WriteStructs(List<byte> bytes, TdfList<TdfStruct> tdf, ITdf parent)
        {
            var writer = TdfWriterFactory.Find(tdf.SubType);
            writer.ParentType = parent.Type;
            writer.ParentName = parent.Label;
            for (var i = 0; i < tdf.Count; i++)
            {
                var item = tdf.Values[i];
                bytes.AddRange(writer.Write(item, true));
            }
        }

        public void WriteTdfs(List<byte> bytes, TdfList<ITdf> tdf, ITdf parent)
        {
            var writer = TdfWriterFactory.Find(tdf.SubType);
            writer.ParentType = parent.Type;
            writer.ParentName = parent.Label;
            for (var i = 0; i < tdf.Count; i++)
            {
                var rawItem = tdf.Values[i];
                if (rawItem.GetType().IsAssignableFrom(typeof (ITdf)))
                {
                    var item = tdf.Values[i];
                    bytes.AddRange(writer.Write(item, false));
                }
            }
        }
    }
}