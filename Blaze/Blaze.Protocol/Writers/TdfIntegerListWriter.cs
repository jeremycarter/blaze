﻿using System.Collections.Generic;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Writers
{
    public class TdfIntegerListWriter : ITdfWriter
    {
        public TdfType ParentType { get; set; }
        public string ParentName { get; set; }

        public List<byte> Write(ITdf input, bool includeHeader)
        {
            var tdf = (TdfIntegerList) input;
            var bytes = new List<byte>();

            if (includeHeader)
            {
                var tagBytes = tdf.Label.ToTag();
                bytes.AddRange(tagBytes);
                bytes.Add((byte) tdf.Type);
            }

            bytes.AddRange(tdf.Count.ToBytes());

            for (var i = 0; i < tdf.Count; i++)
            {
                bytes.AddRange(tdf.Values[i].ToBytes());
            }

            return bytes;
        }
    }
}