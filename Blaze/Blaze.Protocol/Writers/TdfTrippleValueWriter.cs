﻿using System.Collections.Generic;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Writers
{
    public class TdfTrippleValueWriter : ITdfWriter
    {
        public TdfType ParentType { get; set; }
        public string ParentName { get; set; }

        public List<byte> Write(ITdf input, bool includeHeader)
        {
            var tdf = (TdfTrippleValue) input;
            var bytes = new List<byte>();

            if (includeHeader)
            {
                var tagBytes = tdf.Label.ToTag();
                bytes.AddRange(tagBytes);
                bytes.Add((byte) tdf.Type);
            }

            bytes.AddRange(tdf.Value1.ToBytes());
            bytes.AddRange(tdf.Value2.ToBytes());
            bytes.AddRange(tdf.Value3.ToBytes());
            return bytes;
        }
    }
}