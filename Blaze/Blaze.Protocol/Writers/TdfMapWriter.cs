﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Writers
{
    public class TdfMapWriter : ITdfWriter
    {
        public TdfType ParentType { get; set; }
        public string ParentName { get; set; }

        public List<byte> Write(ITdf input, bool includeHeader)
        {
            var tdf = (TdfMap) input;
            tdf.Recount();

            var bytes = new List<byte>();

            if (includeHeader)
            {
                var tagBytes = tdf.Label.ToTag();
                bytes.AddRange(tagBytes);
                bytes.Add((byte) tdf.Type);
            }

            var curHex = bytes.ToHex();

            bytes.Add((byte) tdf.KeyType);
            bytes.Add((byte) tdf.ValueType);
            bytes.AddRange(tdf.Count.ToBytes());

            curHex = bytes.ToHex();

            tdf.Count = tdf.Map.Count;

            Func<TdfType, object, List<byte>> writeListFunc = (type, value) =>
            {
                List<byte> item = null;
                ITdfWriter writer = null;
                switch (type)
                {
                    case TdfType.Integer:
                        var fakeLong = new TdfInteger {Value = Convert.ToInt64(value)};
                        writer = new TdfIntegerWriter();
                        item = writer.Write(fakeLong, false);
                        break;

                    case TdfType.String:
                        var fake = new TdfString {Value = Convert.ToString(value)};
                        writer = new TdfStringWriter();
                        item = writer.Write(fake, false);
                        break;
                    case TdfType.Struct:
                        if (value is TdfStruct)
                        {
                            writer = new TdfStructWriter();
                            item = writer.Write((TdfStruct) value, false);
                            item.RemoveAt(0);
                        }
                        else
                        {
                            writer = TdfWriterFactory.Find(type);
                            item = writer.Write((ITdf) value, false);
                        }
                        break;
                    default:
                        writer = TdfWriterFactory.Find(type);
                        item = writer.Write((ITdf) value, false);
                        break;
                }

                return item;
            };

            for (var i = 0; i < tdf.Count; i++)
            {
                var key = tdf.Map.Keys.ElementAt(i);
                var value = tdf.Map.Values.ElementAt(i);

                var keyBytes = writeListFunc(tdf.KeyType, key);
                var valueBytes = writeListFunc(tdf.ValueType, value);

                if (keyBytes.Any() && valueBytes.Any())
                {
                    bytes.AddRange(keyBytes);

                    curHex = bytes.ToHex();

                    bytes.AddRange(valueBytes);

                    curHex = bytes.ToHex();
                }
            }

            return bytes;
        }
    }
}