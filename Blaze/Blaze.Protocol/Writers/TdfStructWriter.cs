﻿using System.Collections.Generic;
using System.Linq;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Writers
{
    public class TdfStructWriter : ITdfWriter
    {
        public TdfType ParentType { get; set; }
        public string ParentName { get; set; }

        public List<byte> Write(ITdf input, bool includeHeader)
        {
            var tdf = (TdfStruct) input;
            var bytes = new List<byte>();

            if (includeHeader)
            {
                if (tdf.Label != "VALU" || !tdf.StartsWith2)
                {
                    var tagBytes = tdf.Label.ToTag();
                    bytes.AddRange(tagBytes);
                }
            }

            if (tdf.StartsWith2 && tdf.Label == "VALU")
            {
                bytes.Add(2);
            }
            else
            {
                bytes.Add((byte) tdf.Type);
            }

            if (tdf.Values.Any())
            {
                foreach (var val in tdf.Values)
                {
                    var writer = TdfWriterFactory.Find(val.Type);
                    writer.ParentType = tdf.Type;
                    writer.ParentName = tdf.Label;
                    var valBytes = writer.Write(val, true);
                    bytes.AddRange(valBytes);
                }
            }

            bytes.Add(0);
            return bytes;
        }
    }
}