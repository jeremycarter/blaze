﻿using System.Collections.Generic;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Writers
{
    public class TdfHexWriter : ITdfWriter
    {
        public TdfType ParentType { get; set; }
        public string ParentName { get; set; }

        public List<byte> Write(ITdf input, bool includeHeader)
        {
            var tdf = (TdfHex) input;
            var bytes = new List<byte>();
            bytes.AddRange(tdf.Value.ToByteArray());
            return bytes;
        }
    }
}