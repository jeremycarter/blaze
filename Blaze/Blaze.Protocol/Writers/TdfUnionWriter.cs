﻿using System.Collections.Generic;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Writers
{
    public class TdfUnionWriter : ITdfWriter
    {
        public TdfType ParentType { get; set; }
        public string ParentName { get; set; }

        public List<byte> Write(ITdf input, bool includeHeader)
        {
            var tdf = (TdfUnion) input;
            var bytes = new List<byte>();

            if (includeHeader)
            {
                var tagBytes = tdf.Label.ToTag();
                bytes.AddRange(tagBytes);
                bytes.Add((byte) tdf.Type);
            }

            bytes.Add((byte) tdf.SubType);

            if ((byte) tdf.SubType != 0x7F)
            {
                bytes.AddRange(tdf.Content.ToContainer().ToBytes());
            }

            return bytes;
        }
    }
}