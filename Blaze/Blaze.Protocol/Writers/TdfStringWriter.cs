﻿using System.Collections.Generic;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Writers
{
    public class TdfStringWriter : ITdfWriter
    {
        public TdfType ParentType { get; set; }
        public string ParentName { get; set; }

        public List<byte> Write(ITdf input, bool includeHeader)
        {
            var tdf = (TdfString) input;
            var bytes = new List<byte>();

            if (includeHeader)
            {
                var tagBytes = tdf.Label.ToTag();
                bytes.AddRange(tagBytes);
                bytes.Add((byte) tdf.Type);
            }

            int len;
            if (tdf.Value.EndsWith("\0"))
            {
                len = tdf.Value.Length;
            }
            else
            {
                len = tdf.Value.Length + 1;
            }

            bytes.AddRange(len.ToBytes());

            for (var i = 0; i < len - 1; i++)
                bytes.Add((byte) tdf.Value[i]);
            bytes.Add(0);

            return bytes;
        }
    }
}