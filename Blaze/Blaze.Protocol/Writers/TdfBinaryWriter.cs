﻿using System.Collections.Generic;
using System.Linq;
using Blaze.Protocol.Types;

namespace Blaze.Protocol.Writers
{
    public class TdfBinaryWriter : ITdfWriter
    {
        public TdfType ParentType { get; set; }
        public string ParentName { get; set; }

        public List<byte> Write(ITdf input, bool includeHeader)
        {
            var tdf = (TdfBinary) input;
            var bytes = new List<byte>();

            if (includeHeader)
            {
                var tagBytes = tdf.Label.ToTag();
                bytes.AddRange(tagBytes);
                bytes.Add((byte) tdf.Type);
            }

            bytes.AddRange(tdf.Value.Count.ToBytes());
            if (tdf.Value.Any())
            {
                bytes.AddRange(tdf.Value);
            }

            return bytes;
        }
    }
}