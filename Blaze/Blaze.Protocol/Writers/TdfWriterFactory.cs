﻿namespace Blaze.Protocol.Writers
{
    public static class TdfWriterFactory
    {
        public static ITdfWriter Find(TdfType type)
        {
            ITdfWriter writer = null;
            switch (type)
            {
                case TdfType.Integer:
                    writer = new TdfIntegerWriter();
                    break;
                case TdfType.String:
                    writer = new TdfStringWriter();
                    break;
                case TdfType.Binary:
                    writer = new TdfBinaryWriter();
                    break;
                case TdfType.Struct:
                    writer = new TdfStructWriter();
                    break;
                case TdfType.List:
                    writer = new TdfListWriter();
                    break;
                case TdfType.Map:
                    writer = new TdfMapWriter();
                    break;
                case TdfType.Union:
                    writer = new TdfUnionWriter();
                    break;
                case TdfType.IntegerList:
                    writer = new TdfIntegerListWriter();
                    break;
                case TdfType.DoubleValue:
                    writer = new TdfDoubleValueWriter();
                    break;
                case TdfType.TrippleValue:
                    writer = new TdfTrippleValueWriter();
                    break;
                case TdfType.Float:
                    writer = new TdfFloatWriter();
                    break;
                case TdfType.Hexadecimal:
                    writer = new TdfHexWriter();
                    break;
                default:
                    writer = null;
                    break;
            }
            return writer;
        }
    }
}