﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Blaze.Protocol
{
    public class BlazeStream : MemoryStream
    {
        public BlazeStream(byte[] array) : base(array)
        {
        }

        public void SeekUntilNonZero()
        {
            while (this.PeekByte() == 0)
            {
                ReadByte();
            }
        }

        public string ReadString()
        {
            var value = string.Empty;
            var length = (int) ReadInt() - 1;
            if (length > 0)
            {
                var stringBytes = new byte[length];
                Read(stringBytes, 0, length);

                value = Encoding.ASCII.GetString(stringBytes);
            }
            ReadByte();

            return value;
        }

        public long ReadInt()
        {
            var tmp = new List<byte>();
            byte b;
            while ((b = (byte) ReadByte()) >= 0x80)
                tmp.Add(b);
            tmp.Add(b);
            var buff = tmp.ToArray();
            var currshift = 6;
            var result = (ulong) (buff[0] & 0x3F);
            for (var i = 1; i < buff.Length; i++)
            {
                var curbyte = buff[i];
                var l = (ulong) (curbyte & 0x7F) << currshift;
                result |= l;
                currshift += 7;
            }
            return (long) result;
        }

        public uint ReadUInt()
        {
            var buff = this.ReadBytes(4);
            return (uint) ((buff[0] << 24) + (buff[1] << 16) + (buff[2] << 8) + buff[3]);
        }

        public MessageType ReadMessageType()
        {
            var bytes = this.ReadBytes(2);
            return (MessageType) bytes[0];
        }

        public ushort ReadUSingle()
        {
            var bytes = this.ReadBytes(2);
            return (ushort) ((bytes[1] >> 4) << 16);
        }

        public ushort ReadUShort()
        {
            var buff = this.ReadBytes(2);
            if (buff.Length == 0) return 0;
            if (buff.Length == 2) return (ushort) ((buff[0] << 8) + buff[1]);
            return (ushort) (buff[0] << 8);
        }
    }
}