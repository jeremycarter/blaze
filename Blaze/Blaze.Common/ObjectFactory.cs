﻿using System;
using StructureMap;
using StructureMap.Graph;

namespace Blaze.Common
{
    public static class ObjectFactory
    {
        private static Container _container;

        public static IContainer Container
        {
            get
            {
                if (_container == null) return DefaultContainer();
                return _container;
            }
        }

        private static Container DefaultContainer()
        {
            return new Container(c =>
            {
                c.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });
            });
        }

        public static void Build(Registry registry)
        {
            _container = new Container(registry);
        }

        public static void Build(Action<ConfigurationExpression> expression)
        {
            _container = new Container(expression);
        }
    }
}