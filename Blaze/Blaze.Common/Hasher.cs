﻿using System.Security.Cryptography;
using System.Text;

namespace Blaze.Common
{
    public static class Hasher
    {
        public static string SHA1(string input)
        {
            using (var provider = new SHA1Managed())
            {
                var hash = provider.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length*2);
                foreach (var b in hash)
                {
                    sb.Append(b.ToString("x2"));
                }
                return sb.ToString();
            }
        }
    }
}