﻿using System;
using System.Diagnostics;

namespace Blaze.Common
{
    public static class Time
    {
        private static long _initialCount;

        public static long CurrentTime
        {
            get { return (Stopwatch.GetTimestamp() - _initialCount)/(Stopwatch.Frequency/1000); }
        }

        public static void Initialize()
        {
            _initialCount = Stopwatch.GetTimestamp();
        }

        public static DateTime ThreeHoursAgo()
        {
            return DateTime.Now.AddHours(-1);
        }
    }
}