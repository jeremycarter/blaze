﻿using System;
using System.Text;

namespace Blaze.Common.Extensions
{
    public static class ByteExtensions
    {
        public static string ToHex(this byte[] ba)
        {
            if (ba == null) return string.Empty;
            var hex = new StringBuilder(ba.Length*2);
            foreach (var b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public static byte[] ToByteArray(this string hex)
        {
            var numberChars = hex.Length;
            var bytes = new byte[numberChars/2];
            for (var i = 0; i < numberChars; i += 2)
                bytes[i/2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}