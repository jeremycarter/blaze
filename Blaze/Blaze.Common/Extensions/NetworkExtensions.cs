﻿using System;
using System.Linq;
using System.Net;

namespace Blaze.Common.Extensions
{
    public static class NetworkExtensions
    {
        public static long ToLong(this IPAddress address)
        {
            return Convert.ToInt64(address.ToInt());
        }

        public static uint ToInt(this IPAddress address)
        {
            var bytes = address.GetAddressBytes();
            Array.Reverse(bytes); // flip big-endian to little-endian
            return BitConverter.ToUInt32(bytes, 0);
        }

        public static IPAddress ToIPAddress(this long address)
        {
            var bytes = BitConverter.GetBytes(address);
            if (bytes.Length == 8)
            {
                var trimmed = bytes.TakeWhile((v, index) => bytes.Skip(index).Any(w => w != 0x00)).ToArray();
                Array.Reverse(trimmed);
                return new IPAddress(trimmed);
            }
            return new IPAddress(Convert.ToInt64(address));
        }
    }
}