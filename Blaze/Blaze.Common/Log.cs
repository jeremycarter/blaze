﻿using System;
using System.IO;

namespace Blaze.Common
{
    public static class Log
    {
        public static void Error(Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(ex.Message);
            Console.ResetColor();
        }

        public static void Warn(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public static void Request(string message)
        {
            Info(message, ConsoleColor.Green);
        }

        public static void Info(string message, ConsoleColor color = ConsoleColor.White)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public static void Bytes(byte[] bytes, string prepend)
        {
            try
            {
                var ticks = DateTime.Now.Ticks;
                var dir = Path.Combine(Environment.CurrentDirectory, "Requests");
                if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
                File.WriteAllBytes(Path.Combine(dir, $"{ticks}_{prepend}_RAW.bin"), bytes);
            }
            catch (Exception ex)
            {
                Error(ex);
            }
        }

        public static void Bytes(byte[] bytes, ushort component, ushort command, ushort type = 0, int clientType = 0)
        {
            var prepend = "CLNT";
            if (clientType == 2) prepend = "SVR";
            try
            {
                var messageType = "REPLY";
                switch (type)
                {
                    case 0:
                    case 16:
                        messageType = "MSG";
                        break;
                    case 8192:
                        messageType = "NOTIFICATION";
                        break;
                    case 12288:
                        messageType = "ERROR";
                        break;
                }

                var ticks = DateTime.Now.Ticks;
                var dir = Path.Combine(Environment.CurrentDirectory, "Packets");
                if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
                File.WriteAllBytes(Path.Combine(dir, $"{ticks}_{prepend}_{messageType}_{component:X02}_{command:X02}.bin"), bytes);
            }
            catch (Exception ex)
            {
                Error(ex);
            }
        }
    }
}