﻿using System;
using System.Runtime.Caching;

namespace Blaze.Common
{
    public class Cache
    {
        private const int DefaultMinutes = 15;
        private readonly MemoryCache _cache;

        public Cache()
        {
            _cache = new MemoryCache("Cache");
        }

        public bool Remove(string key)
        {
            _cache.Remove(key);
            return true;
        }

        public bool Exists(string key)
        {
            return _cache.Contains(key);
        }

        public bool Set<T>(string key, T item)
        {
            return _cache.Add(new CacheItem(key, item),
                new CacheItemPolicy
                {
                    SlidingExpiration = TimeSpan.FromMinutes(DefaultMinutes)
                }
                );
        }

        public T Get<T>(string key)
        {
            if (_cache.Contains(key))
            {
                return (T) _cache.Get(key);
            }
            return default(T);
        }
    }
}