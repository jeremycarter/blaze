﻿using Blaze.DataBridge.Entities;

namespace Blaze.DataBridge.MySql
{
    public class MySqlDataBridge : IDataBridge
    {
        public Match SaveOrUpdateMatch(Match match)
        {
            return null;
        }

        public Game SaveOrUpdateServer(Game game)
        {
            return null;
        }

        public Game GetGameById(ulong id)
        {
            return null;
        }

        public User SaveOrUpdateUser(User match)
        {
            return null;
        }

        public User GetUserByEmail(string email)
        {
            return null;
        }

        public User GetUserByEmailAndPassword(string email, string passwordHash)
        {
            return null;
        }

        public User GetUserByPersonaId(ulong personaId)
        {
            return null;
        }

        public User GetUserById(ulong id)
        {
            return null;
        }

        public void DeleteGameById(ulong id)
        {
        }
    }
}