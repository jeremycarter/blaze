﻿using System;

namespace Blaze.Networking
{
    public static class Utilities
    {
        public static ulong GetUnixTime()
        {
            return (ulong) DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
        }

        public static uint SwapBytes(uint word)
        {
            // convert big endian to little endian
            return ((word >> 24) & 0x000000FF) | ((word >> 8) & 0x0000FF00) | ((word << 8) & 0x00FF0000) |
                   ((word << 24) & 0xFF000000);
        }
    }
}