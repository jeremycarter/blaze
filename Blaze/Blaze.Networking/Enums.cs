﻿using System;

namespace Blaze.Networking
{
    public enum Product
    {
        Battlefield3,
        Battlefield4,
        MedalOfHonour,
        MassEffect3,
        BattlefieldHardline,
        StarWarsBattlefront,
        Battlefield1,
        Unknown = 99
    }

    [Flags]
    public enum ClientType : ushort
    {
        GameplayUser,
        HttpUser,
        DedicatedServer,
        Tools,
        Invalid,
        Any = GameplayUser | HttpUser | DedicatedServer | Tools
    }
}