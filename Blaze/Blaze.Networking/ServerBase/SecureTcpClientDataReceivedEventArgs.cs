﻿namespace Blaze.Networking.ServerBase
{
    public class SecureTcpClientDataReceivedEventArgs
    {
        public byte[] Bytes { get; set; }
        public int Length { get; set; }
        public SecureTcpClient Client { get; set; }
        public ServerDetails Server { get; set; }
    }
}