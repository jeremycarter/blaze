﻿using System.Net;
using System.Threading.Tasks;
using Blaze.Networking.Components;
using Blaze.Protocol;

namespace Blaze.Networking.ServerBase
{
    public interface ISecureTcpClient
    {
        ServerDetails Server { get; set; }
        IPEndPoint EndPoint { get; set; }
        long? SessionId { get; set; }

        void SendBytes(byte[] bytes);
        void Notify(INotification notification);
        void Send(BlazePacket packet);
    }
}