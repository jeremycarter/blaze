﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Blaze.Common;
using Blaze.Networking.Components;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.ServerBase
{
    public class BlazeClientRequest
    {
        public BlazeClientRequest(BlazePacket packet)
        {
            Packet = packet;
        }

        public BlazePacket Packet { get; set; }
        public SecureTcpClient Client { get; set; }
        public ServerDetails Server { get; set; }
        public ISessionObject Session { get; set; }

        public ClientType ClientType()
        {
            return Session?.ClientType ?? Networking.ClientType.GameplayUser;
        }

        public void ReplyEmpty()
        {
            Reply();
        }

        public void ReplyError()
        {
            Reply(null, 1);
        }

        public void ReplyPacket(BlazePacket packet)
        {
            Log.Info($"Sending reply {packet.Component} {packet.Command}");
            Send(packet);
        }

        public void Reply(List<ITdf> data = null, ushort error = 0)
        {
            var packet = new BlazePacket();
            packet.Component = Packet.Component;
            packet.Command = Packet.Command;
            packet.Error = error;
            packet.MessageType = (ushort)MessageType.Reply;
            packet.Id = Packet.Id;
            packet.Content = new TdfContainer(data);

            Log.Info($"Sending reply {packet.Component} {packet.Command}");

            var bytes = packet.ToBytes();
            Log.Bytes(bytes, packet.Component, packet.Command, packet.MessageType, (int)ClientType());
            SendBytes(bytes);
        }
        public void Reply(ushort command, List<ITdf> data = null, ushort error = 0)
        {
            var packet = new BlazePacket();
            packet.Component = Packet.Component;
            packet.Command = command;
            packet.Error = error;
            packet.MessageType = (ushort)MessageType.Reply;
            packet.Id = Packet.Id;
            packet.Content = new TdfContainer(data);

            Log.Info($"Sending reply {packet.Component} {packet.Command}");

            var bytes = packet.ToBytes();
            Log.Bytes(bytes, packet.Component, packet.Command, packet.MessageType, (int)ClientType());
            SendBytes(bytes);
        }

        public void Notify(INotification notification)
        {
            var packet = new BlazePacket();
            packet.Component = notification.Component;
            packet.Command = notification.Command;
            packet.Error = 0;
            packet.MessageType = (ushort)MessageType.Notification;
            packet.Id = Packet.Id;
            packet.Content = notification.Container();

            Log.Info($"Sending notification {packet.Component} {packet.Command} to {Client.EndPoint}");

            var bytes = packet.ToBytes();
            Log.Bytes(bytes, packet.Component, packet.Command, packet.MessageType, (int)ClientType());
            Client.SendBytesSync(bytes);
        }

        private void SendBytes(byte[] bytes)
        {
            Client.SendBytesSync(bytes);
        }

        private void Send(BlazePacket packet)
        {
            var bytes = packet.ToBytes();

            Log.Info(
                $"Sending packet {packet.MessageType} {packet.Component} {packet.Command} size {bytes.Length} to {Client.EndPoint}");
            Log.Bytes(bytes, packet.Component, packet.Command, packet.MessageType, (int)ClientType());
            Client.SendBytes(bytes);
        }
    }
}