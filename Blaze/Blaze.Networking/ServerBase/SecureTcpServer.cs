﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using Blaze.Common;
using Blaze.Common.Extensions;

namespace Blaze.Networking.ServerBase
{
    public class SecureTcpServer
    {
        private static bool _acceptClients;
        private CancellationTokenSource _cancellation;
        private TcpListener _tcpListener;
        public X509Certificate2 Certificate;

        public SecureTcpServer(string hostName, IPAddress ipAddress, int port, X509Certificate2 certificate)
        {
            Port = port;
            Certificate = certificate;

            ServerDetails = new ServerDetails
            {
                HostName = hostName,
                IPAddress = ipAddress,
                Port = port
            };
        }

        public ServerDetails ServerDetails { get; set; }
        public int Port { get; set; }
        public string Name { get; set; }

        public event EventHandler<SecureTcpClientDataReceivedEventArgs> ClientBytesReceived;

        public async Task Start()
        {
            if (_tcpListener == null)
            {
                _tcpListener = TcpListener.Create(Port);
                _tcpListener.Server.SetSocketOption(SocketOptionLevel.IPv6, SocketOptionName.IPv6Only, false);
                _tcpListener.Start();

                _cancellation = new CancellationTokenSource();
                _acceptClients = true;

                Log.Info($"[{Name}] Server is now listening on port {Port}");

                await AcceptTcpClientsAsync();
            }
        }

        public void Stop()
        {
            _acceptClients = false;
            if (_cancellation != null && !_cancellation.IsCancellationRequested)
            {
                _cancellation.Cancel();
            }
        }

        private async Task AcceptTcpClientsAsync()
        {
            while (_acceptClients)
            {
                try
                {
                    var tcpClient = await Task.Run(() => _tcpListener.AcceptTcpClientAsync(), _cancellation.Token);

                    Log.Info($"[{Name}] A client is available");
                    try
                    {
                        var client = new SecureTcpClient(this)
                        {
                            Tcp = tcpClient,
                            EndPoint = (IPEndPoint) tcpClient.Client.RemoteEndPoint,
                            Server = ServerDetails
                        };

                        Task.Factory.StartNew(() => HandleClient(client), _cancellation.Token,
                            TaskCreationOptions.LongRunning, TaskScheduler.Default).Continue();
                    }
                    catch (Exception clientException)
                    {
                        Log.Error(clientException);
                        tcpClient.Client.Shutdown(SocketShutdown.Both);
                        tcpClient.Client.Close();
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            }
        }

        private async void HandleClient(SecureTcpClient client)
        {
            try
            {
                client.BytesReceived += (o, args) => { ClientBytesReceived?.Invoke(this, args); };

                await client.AuthenticateStream();

                await client.ReceiveData();
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                try
                {
                    if (client.Tcp.Connected && client.Tcp.Available > 0)
                    {
                        HandleClient(client);
                    }
                    else
                    {
                        client.Disconnect();
                    }
                }
                catch (Exception)
                {
                    client = null;
                }
            }
        }
    }
}