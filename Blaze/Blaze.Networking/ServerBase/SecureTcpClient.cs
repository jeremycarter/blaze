﻿using System;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Blaze.Common;
using Blaze.Common.Extensions;
using Blaze.Networking.Components;
using Blaze.Networking.Extensions;
using Blaze.Protocol;

namespace Blaze.Networking.ServerBase
{
    public class SecureTcpClient : ISecureTcpClient
    {
        public SecureTcpClient(SecureTcpServer tcpServer)
        {
            _tcpServer = tcpServer;
        }

        private SecureTcpServer _tcpServer { get; }
        public TcpClient Tcp { get; set; }
        public SslStream Stream { get; set; }
        public DateTime LastMessageReceived { get; set; }
        public ServerDetails Server { get; set; }
        public long? SessionId { get; set; }
        public IPEndPoint EndPoint { get; set; }

        public void SendBytesSync(byte[] bytes, int attempts = 0)
        {
            if (Tcp == null) return;
            while (!Stream.CanWrite)
            {
                if (!Tcp.Connected) return;
                // Loop until the stream can write
            }

            if (Stream.CanWrite)
            {
                Stream.Write(bytes, 0, bytes.Length);
                Stream.Flush();
            }
            else
            {
                attempts += 1;
                if (attempts < 3)
                {
                    // Recursive loop
                    SendBytesSync(bytes, attempts);
                }
            }
        }

        public void SendBytes(byte[] bytes)
        {
            try
            {
                if (Tcp == null) return;
                while (!Stream.CanWrite)
                {
                    if (!Tcp.Connected) return;
                    // Loop until the stream can write
                }

                if (Stream.CanWrite)
                {
                    Stream.Write(bytes, 0, bytes.Length);
                    Stream.Flush();
                }
                else
                {
                    // Recursive loop
                    SendBytes(bytes);
                }
            }
            catch (NotSupportedException)
            {
                // Recursive loop
                SendBytes(bytes);
            }

        }

        public void Notify(INotification notification)
        {
            var packet = new BlazePacket();
            packet.Component = notification.Component;
            packet.Command = notification.Command;
            packet.Error = 0;
            packet.MessageType = (ushort)MessageType.Notification;
            packet.Id = 0;
            packet.Content = notification.Container();

            Log.Info($"Sending notification {packet.Component} {packet.Command} to {EndPoint}");

            var bytes = packet.ToBytes();
            Log.Bytes(bytes, packet.Component, packet.Command, packet.MessageType, 2);

            SendBytesSync(packet.ToBytes());
        }

        public void Send(BlazePacket packet)
        {
            var bytes = packet.ToBytes();
            Log.Info(
                $"Sending packet {packet.MessageType} {packet.Component} {packet.Command} size {bytes.Length} to {EndPoint}");
            SendBytes(bytes);
        }

        public event EventHandler<SecureTcpClientDataReceivedEventArgs> BytesReceived;

        public async Task<SslStream> AuthenticateStream()
        {
            if (Stream != null) return Stream;

            var stream = new SslStream(Tcp.GetStream(), false, VerifyServerCertificate, UserCertificateSelectionCallback);

            await stream.AuthenticateAsServerAsync(_tcpServer.Certificate, false, SslProtocols.Default, false)
                .TimeoutAfter(TimeSpan.FromSeconds(5));

            Stream = stream;
            return Stream;
        }

        public async Task ReceiveData()
        {
            while (Tcp.Client.Connected)
            {
                try
                {
                    var bytes = await Tcp.ReadAllBytesAsync(Stream);

                    if (bytes != null && bytes.Length > 0)
                    {
                        BytesReceived?.Invoke(this, new SecureTcpClientDataReceivedEventArgs
                        {
                            Length = bytes.Length,
                            Bytes = bytes,
                            Client = this,
                            Server = Server
                        });
                    }
                    else
                    {
                        // Give the client 2000ms to see if its still connected
                        if (Tcp.Client.Poll(2000, SelectMode.SelectRead) && Tcp.Client.Available == 0)
                        {
                            Disconnect();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    if (Tcp.Client.Connected)
                    {
                        await ReceiveData();
                    }
                }
            }
        }

        public void Disconnect()
        {
            try
            {
                Tcp.Client.Shutdown(SocketShutdown.Both);
                Tcp.Close();
            }
            catch (Exception)
            {
                Tcp?.Close();
            }
        }

        private X509Certificate UserCertificateSelectionCallback(object sender, string targetHost,
            X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
        {
            return _tcpServer.Certificate;
        }

        private bool VerifyServerCertificate(object sender, X509Certificate certificate, X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    }
}