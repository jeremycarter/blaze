﻿using System.Net;

namespace Blaze.Networking.ServerBase
{
    public class ServerDetails
    {
        public string HostName { get; set; }
        public int Port { get; set; }
        public IPAddress IPAddress { get; set; }
    }
}