﻿using System;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using Blaze.Common;
using Blaze.Networking.Components;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking
{
    public class BlazeServer
    {
        private readonly CancellationTokenSource _cancellationToken;
        private readonly BlazeStreamParser _parser;
        private readonly Product _product;
        private readonly SecureTcpServer _tcpServer;
        private RequestMediator _mediator;

        public BlazeServer(string hostName, IPAddress ipAddress, X509Certificate2 certificate, int port, Product product)
        {
            _cancellationToken = new CancellationTokenSource();
            _tcpServer = new SecureTcpServer(hostName, ipAddress, port, certificate);
            _tcpServer.Name = product.ToString();
            _tcpServer.ClientBytesReceived += OnClientBytesReceived;
            _product = product;
            _parser = new BlazeStreamParser();
        }

        public Action<BlazeClientRequest> ResolveClientSession { get; set; }

        public void ScanForCommandHandlersIn(string ns)
        {
            _mediator = new RequestMediator();
            _mediator.Scan(ns);
        }

        public void Start()
        {
            Task.Factory.StartNew(() => _tcpServer.Start(), _cancellationToken.Token,
                TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        public void Stop()
        {
            _tcpServer.Stop();
            _cancellationToken.Cancel();
        }

        private void OnClientBytesReceived(object sender, SecureTcpClientDataReceivedEventArgs args)
        {
            try
            {
                args.Client.LastMessageReceived = DateTime.Now;

                var packet = _parser.Parse(args.Bytes);
                if (packet != null && packet.Header.Length > 10) // Valid blaze packet
                {
                    var request = new BlazeClientRequest(packet);
                    request.Client = args.Client;
                    request.Server = args.Server;

                    if (request.Session == null)
                    {
                        ResolveClientSession?.Invoke(request);
                    }
                    if (request.Session != null) request.Session.LastSeen = args.Client.LastMessageReceived;

                    if (request.Session != null)
                    {
                        Log.Bytes(args.Bytes, packet.Component, packet.Command,
                            packet.MessageType, (int) request.Session.ClientType);
                    }
                    else
                    {
                        Log.Bytes(args.Bytes, packet.Component, packet.Command,
                            packet.MessageType);
                    }

                    Log.Info(
                        $"A client from {args.Client.EndPoint} is sending a request {packet.Component}:{packet.Command}");

                    try
                    {
                        _mediator.Handle(request);
                    }
                    catch (MissingMethodException ex)
                    {
                        Log.Error(ex);
                        // Reply with a null message
                        request.Reply();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }
    }
}