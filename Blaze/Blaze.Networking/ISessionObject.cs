﻿using System;
using System.Collections.Generic;

namespace Blaze.Networking
{
    public interface ISessionObject
    {
        long Id { get; set; }
        string Key { get; set; }

        ClientType ClientType { get; set; }
        DateTime LastSeen { get; set; }
        Dictionary<string, object> Data { get; set; }
    }
}