﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Blaze.Networking.Extensions
{
    public static class StreamExtensions
    {
        public static byte[] ReadAllBytes(this TcpClient client, Stream stream)
        {
            try
            {
                var bytes = new byte[0];
                if (stream.CanRead)
                {
                    var readBuffer = new byte[client.ReceiveBufferSize];
                    using (var writer = new MemoryStream())
                    {
                        do
                        {
                            var bytesRead = stream.Read(readBuffer, 0, readBuffer.Length);
                            if (bytesRead <= 0)
                            {
                                break;
                            }
                            writer.Write(readBuffer, 0, bytesRead);
                        } while (client.Available > 0);
                        bytes = writer.ToArray();
                    }
                }
                return bytes;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static async Task<byte[]> ReadAllBytesAsync(this TcpClient client, Stream stream)
        {
            try
            {
                var bytes = new byte[0];
                if (stream.CanRead)
                {
                    var readBuffer = new byte[client.ReceiveBufferSize];
                    using (var writer = new MemoryStream())
                    {
                        do
                        {
                            try
                            {
                                if (!client.Connected) break;
                                var bytesRead = await stream.ReadAsync(readBuffer, 0, readBuffer.Length);
                                if (bytesRead <= 0)
                                {
                                    break;
                                }
                                await writer.WriteAsync(readBuffer, 0, bytesRead);
                            }
                            catch (IOException)
                            {
                                return null;
                            }
                        } while (client.Available > 0);
                        bytes = writer.ToArray();
                    }
                }
                return bytes;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}