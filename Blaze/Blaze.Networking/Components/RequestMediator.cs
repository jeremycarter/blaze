﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blaze.Common;
using Blaze.Networking.Clients;
using Blaze.Networking.ServerBase;

namespace Blaze.Networking.Components
{
    public class RequestMediator
    {
        public RequestMediator()
        {
            Handlers = new List<ComponentCommandDefinition>();
        }

        private List<ComponentCommandDefinition> Handlers { get; }

        public void Scan(string nameSpace = "Blaze.Networking")
        {
            var handlerType = typeof (ICommandHandler);

            var allBlazeTypes =
                AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(a => a.GetTypes().Where(i => i.Namespace != null && i.Namespace.StartsWith("Blaze")));

            var types = allBlazeTypes.Where(p => handlerType.IsAssignableFrom(p)
                                                 && p.Namespace.StartsWith(nameSpace)).ToList();

            foreach (var type in types)
            {
                try
                {
                    var componentAttr =
                        (ComponentAttribute) type.GetCustomAttributes(typeof (ComponentAttribute), true)[0];
                    var commandAttr = (CommandAttribute) type.GetCustomAttributes(typeof (CommandAttribute), true)[0];
                    if (componentAttr != null && commandAttr != null)
                    {
                        var instance = ObjectFactory.Container.GetInstance(type);
                        Handlers.Add(new ComponentCommandDefinition
                        {
                            Component = componentAttr.Component,
                            Command = commandAttr.Command,
                            Handler = (ICommandHandler) instance
                        });
                    }
                }
                catch (Exception)
                {
                    Log.Info($"Failed to load ICommandHandler of type {type.Name}");
                }
            }
        }

        public ICommandHandler Find(BlazeClientRequest request)
        {
            var def =
                Handlers.FirstOrDefault(
                    i => i.Component == request.Packet.Component && i.Command == request.Packet.Command);
            return def?.Handler;
        }

        public void Handle(BlazeClientRequest request)
        {
            try
            {
                var handler = Find(request);
                if (handler != null)
                {
                    handler.Handle(request, request.Packet);
                }
                else
                {
                    throw new MissingMethodException(
                        $"Mediator could not find a handler for component {request.Packet.Component} {request.Packet.Command}");
                }
            }
            catch (Exception ex)
            {
                Log.Info($"Error handling component {request.Packet.Component} {request.Packet.Command}");
                Log.Error(ex);
                request.Reply();
            }
        }
    }
}