﻿using Blaze.Protocol;

namespace Blaze.Networking.Components
{
    public interface INotification
    {
        ushort Component { get; set; }
        ushort Command { get; set; }
        TdfContainer Container();
    }
}