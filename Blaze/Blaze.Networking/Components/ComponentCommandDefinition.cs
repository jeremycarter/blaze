﻿namespace Blaze.Networking.Components
{
    public class ComponentCommandDefinition
    {
        public ICommandHandler Handler { get; set; }
        public int Command { get; set; }
        public int Component { get; set; }
    }
}