﻿using System.Threading.Tasks;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;

namespace Blaze.Networking.Components
{
    public interface ICommandHandler
    {
        void Handle(BlazeClientRequest request, BlazePacket packet);
    }
}