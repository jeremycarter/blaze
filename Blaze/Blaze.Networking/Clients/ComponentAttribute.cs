﻿using System;

namespace Blaze.Networking.Clients
{
    public class ComponentAttribute : Attribute
    {
        public ComponentAttribute(object component)
        {
            Component = (ushort) component;
        }

        public ComponentAttribute(int component)
        {
            Component = component;
        }

        public int Component { get; }
    }
}