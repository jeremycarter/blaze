﻿using System;

namespace Blaze.Networking.Clients
{
    public class CommandAttribute : Attribute
    {
        public CommandAttribute(int command)
        {
            Command = command;
        }

        public int Command { get; set; }
    }
}