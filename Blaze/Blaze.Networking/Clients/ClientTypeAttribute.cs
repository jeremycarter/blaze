﻿using System;

namespace Blaze.Networking.Clients
{
    public class ClientTypeAttribute : Attribute
    {
        public ClientTypeAttribute(ClientType type)
        {
            ClientType = type;
        }

        public ClientType ClientType { get; set; }
    }
}