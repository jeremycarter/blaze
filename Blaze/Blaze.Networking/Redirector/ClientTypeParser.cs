﻿namespace Blaze.Networking.Redirector
{
    public static class ClientTypeParser
    {
        public static ClientType Parse(string input)
        {
            if (input.Contains("server"))
            {
                return ClientType.DedicatedServer;
            }
            if (input.Contains("client") | input.Contains("game"))
            {
                return ClientType.GameplayUser;
            }
            return ClientType.Invalid;
        }
    }
}