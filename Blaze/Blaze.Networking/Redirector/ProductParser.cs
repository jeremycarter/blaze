﻿namespace Blaze.Networking.Redirector
{
    public static class ProductParser
    {
        public static Product Parse(string input)
        {
            if (input.Contains("battlefield-4"))
            {
                return Product.Battlefield4;
            }
            if (input.Contains("battlefield-3"))
            {
                return Product.Battlefield3;
            }
            return Product.Unknown;
        }
    }
}