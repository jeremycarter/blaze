﻿namespace Blaze.Networking.Redirector
{
    public class ServiceRedirection
    {
        public ClientType ClientType { get; set; }
        public Product Product { get; set; }
        public int Port { get; set; }
    }
}