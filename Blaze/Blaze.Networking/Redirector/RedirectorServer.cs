﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using Blaze.Common;
using Blaze.Common.Extensions;
using Blaze.Networking.ServerBase;
using Blaze.Protocol;
using Blaze.Protocol.Types;

namespace Blaze.Networking.Redirector
{
    public class RedirectorServer
    {
        private readonly CancellationTokenSource _cancellationToken;
        private readonly BlazeStreamParser _parser;
        private readonly List<ServiceRedirection> _redirections;
        private readonly SecureTcpServer _tcpServer;

        public RedirectorServer(string hostName, IPAddress ipAddress, X509Certificate2 certificate, int port = 42127)
        {
            _cancellationToken = new CancellationTokenSource();
            _tcpServer = new SecureTcpServer(hostName, ipAddress, port, certificate);
            _tcpServer.Name = "GOSRedirector";
            _tcpServer.ClientBytesReceived += OnClientBytesReceived;
            _redirections = new List<ServiceRedirection>();
            _parser = new BlazeStreamParser();
        }

        public void Start()
        {
            Task.Factory.StartNew(() => _tcpServer.Start(), _cancellationToken.Token,
                TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        public void Stop()
        {
            _tcpServer.Stop();
            _cancellationToken.Cancel();
        }

        public void Add(ServiceRedirection redirection)
        {
            _redirections.Add(redirection);
        }

        private void OnClientBytesReceived(object sender, SecureTcpClientDataReceivedEventArgs args)
        {
            Log.Info($"[{_tcpServer.Name}] Received {args.Length} bytes from client {args.Client.EndPoint}");

            var packet = _parser.Parse(args.Bytes);
            if (packet != null)
            {
                var request = new BlazeClientRequest(packet);
                request.Client = args.Client;
                request.Server = args.Server;

                try
                {
                    if (packet.Component == 5)
                    {
                        var product = ProductParser.Parse(packet.Content.GetString("NAME"));
                        var client = ClientTypeParser.Parse(packet.Content.GetString("CLNT"));

                        if (product != Product.Unknown && client != ClientType.Invalid)
                        {
                            var lookup =
                                _redirections.FirstOrDefault(i => i.Product == product && i.ClientType == client) ??
                                _redirections.FirstOrDefault(i => i.Product == product);

                            if (lookup != null)
                            {
                                request.Reply(new List<ITdf>
                                {
                                    new TdfUnion("ADDR", 0,
                                        new TdfStruct("VALU", new List<ITdf>
                                        {
                                            new TdfString("HOST", _tcpServer.ServerDetails.IPAddress.ToString()),
                                            new TdfInteger("IP", _tcpServer.ServerDetails.IPAddress.ToInt()),
                                            new TdfInteger("PORT", lookup.Port)
                                        }, false)
                                        ),
                                    new TdfInteger("SECU", 1),
                                    new TdfInteger("XDNS", 0)
                                });
                            }
                        }
                    }
                }
                catch (MissingMethodException ex)
                {
                    Log.Error(ex);
                }
            }
        }
    }
}