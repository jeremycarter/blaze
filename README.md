# Blaze Server

### Outcomes
* A blaze server that supports multiple products (such as BF3,BF4,MoH)
* A blaze server that is modular for developers
* A blaze server that doesnt need a database server
* A blaze server that can be run on LAN
* A blaze server that has no dependencies except for .NET framework

### Use the following
* SOLID programming principles
* Structuremap
* LiteDb

### Special

* Sebastian's great BF3 implementation (used as a base) https://github.com/SebastianMalek/blaze-server